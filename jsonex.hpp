//
// jsonex.h
//
//  Created by Seth Hill on 2/28/14.
//  Copyright (c) 2014 Seth Hill. All rights reserved.
//
// Experimental C++ JSON Interface, inspired by cJSON

#ifndef json_experiment_hpp
#define json_experiment_hpp

#include <map> 
#include <memory>
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
using std::cout;

// Debugging tricks
// Eat debug messages
#define debug(x) 0
// Print debug messages
//#define debug(x) cout << x;
// ---


// todo: move to_string etc. functions into jsonex_utilities
#include "jsonex_utilities.h"
#include <utility>

namespace jsonex {
//    using std::map;
    using std::initializer_list;
    using std::pair;
    using std::string;
    using std::vector;
    
    typedef double default_double_type;
    typedef int default_int_type;
    typedef string default_string_type;
    
    // Assign default template types
    template <typename double_type_=default_double_type,
              typename int_type_=default_int_type,
              typename string_type_=default_string_type>
    class basic_object;

    // Default type of object
    typedef basic_object<> object;

    // Helper class for array initialization
    template <typename double_type_=default_double_type,
              typename int_type_=default_int_type,
              typename string_type_=default_string_type>
    class basic_array {
    public:
        using object_ = basic_object<double_type_, int_type_, string_type_>;
        
        vector<object_> children;
        
        basic_array(std::initializer_list<object_> objects) :
            children(objects) {}
        
        basic_array(std::vector<object_> objects) :
            children(objects) {}
    };
    typedef basic_array<> array;
    
    template <typename double_type_=default_double_type,
              typename int_type_=default_int_type,
              typename string_type_=default_string_type>
    class basic_object_reference {
    public:
        using object_ = basic_object<double_type_, int_type_, string_type_>;
        object_ *object;
        
        basic_object_reference(object_ *o) {
            object = o;
        }
        
        operator bool() const {
            return object != nullptr;
        }

        bool operator !() const {
            return !object;
        }
        
        object_ *operator ->() const {
            if (object != nullptr) {
                return object;
            }
            else {
                throw std::runtime_error("Non-dereferenceable object");
            }
        }
        
        object_ &operator *() const {
            if (object != nullptr) {
                return *object;
            }
            else {
                throw std::runtime_error("Non-dereferenceable object");
            }
        }
    };
    
    // default object reference type
    typedef basic_object_reference<> object_reference;
    
    
    enum kind {
        // kind_invalid used to mark an object that doesn't exist. Use
        // object.is_valid() to examine this value. This is used as a return
        // value from the get*() commands to indicate that the object you asked
        // for doesn't exist.
        kind_invalid,
        kind_false,
        kind_true,
        kind_null,
        kind_number,
        kind_string,
        kind_array,
        kind_object
    };
    
    template <typename double_type_, typename int_type_, typename string_type_>
    class basic_object {
    public:
        typedef double_type_ double_type;
        typedef int_type_ int_type;
        typedef string_type_ string_type;
        
        typedef vector<double_type> double_vector;
        typedef vector<int_type> int_vector;
        typedef vector<string_type> string_vector; 

        // shorthand typedef for this template's instantiation of basic_object
        using array_ = typename jsonex::basic_array<double_type, int_type, string_type>;
        using object_ = typename jsonex::basic_object<double_type, int_type, string_type>;
        using object_reference_ = typename jsonex::basic_object_reference<double_type, int_type, string_type>;
//        typedef basic_object<double_type, int_type, string_type> object_;

//        typedef vector<object_> object_data;
//        typedef map<string_type, object_> object_data;
//        using object_data = typename std::map<string_type, object_>;
//        using object_data_iterator = typename object_data::iterator;
//        using object_data_const_iterator = typename object_data::const_iterator;

        
        /* Try a vector of objects, with keys stored in the data structure's 
         name parameter. This is similar to the implementation in cJSON, except 
         cJSON uses a doubly-linked list. This will probably be slower for 
         insertion and deletion, faster (than a linked list) for element 
         access, slower than a map for element access. However, it my 
         assumption that since JSON objects are serialization formats, they 
         won't be mutated much - they are created from a json string, then 
         values are read off, or they are constructed from object data, 
         converted to a string and then sent over the wire. Mutation may happen, 
         but it should happen infrequently so speed is not as much of an issue.
         Large objects that are mutated will suffer a greater penalty though. 
         Perhaps I will template the class to allow the user to select an 
         implementation as a std::list or something if the user knows the 
         objects will be large ... ? */
        using object_data = typename std::vector<object_>;
        using object_data_iterator = typename object_data::iterator;
        using object_data_const_iterator = typename object_data::const_iterator;
        
        string_type name;
        
        kind object_kind;
        double_type double_value;
        int_type int_value;
        string_type string_value;
        
//        object_data children;
        std::vector<object_> children;
        
        /** Return an invalid object */
        static const object_ invalid_object() {
            object_ o;
            o.object_kind = kind_invalid;
            return o;
        }
        
        static object_ create_object() {
            object_ o;
            o.object_kind = kind_object;
            return o;
        }
        
        static object_ create_array() {
            object_ o;
            o.object_kind = kind_array;
            return o;
        }
        
        /** Default constructor. Yields a null, unnamed object */
        basic_object() :
            name(""),
            object_kind(kind_null),
            double_value(0),
            int_value(0),
            string_value("")
        {
            debug("default constructor invoked " << std::hex << this << "\n");
            object_kind = kind_null;
        }
        
        ~basic_object() {
            debug("destructor invoked " << std::hex << this << "\n");
            // nothing to do
        }
       
        basic_object(object_ &other) :
            name(other.name),
            object_kind(other.object_kind),
            double_value(other.double_value),
            int_value(other.int_value),
            string_value(other.string_value),
            children(other.children)
        {
            debug("nonconst copy constructor invoked " << std::hex << this << "\n");
        }

        basic_object(const object_ &other) :
            name(other.name),
            object_kind(other.object_kind),
            double_value(other.double_value),
            int_value(other.int_value),
            string_value(other.string_value),
            children(other.children)
        {
            debug("const copy constructor " << std::hex << this << " = " << std::hex << &other << "\n");
        }
        
        /** move constructor. Note: noexcept is required, otherwise the compiler
            will prefer the copy constructor */
        basic_object(object_ &&other) noexcept :
            name(std::move(other.name)),
            object_kind(other.object_kind),
            double_value(other.double_value),
            int_value(other.int_value),
            string_value(std::move(other.string_value)),
            children(std::move(other.children))
        {
            debug("move constructor invoked " << std::hex << this << " = " << &other << "\n");

            // Set the other as invalid, since the life has been sucked out of it
            other.object_kind = kind_invalid;
        }
        
        // const vector<pair<string, object>> constructor
        //        basic_object(const vector<pair<string_type, const basic_object<double_type, int_type, string_type>>> &children){
        //            debug_stream << "const vector<pair<string, object>> constructor invoked " << std::hex << this << "\n";
        //            add_children(children);
        //        }

        basic_object(const array_ &arr) : basic_object()
        {
            object_kind = kind_array;
            children = arr.children;
        }
        
        // array initializer list constructor
//        basic_object(std::initializer_list<object_> init) :
//            object_kind(kind_array),
//            children(init)
//        {
//            debug("array initializer_list constructor invoked " << std::hex << this << "\n");
//        }

        // object initializer list constructor
        basic_object(initializer_list<pair<string_type, object_>> init) :
            name(""),
            double_value(0),
            int_value(0),
            string_value(""),
            object_kind(kind_object)
        {
            debug("object initializer_list constructor invoked " << std::hex << this << "\n");
            children.reserve(init.size());
            for (const pair<string_type, object_> &item: init) {
//                Stuff the item int the vector, then set it's name
                children.push_back(item.second);
                children.back().name = item.first;

                // Alternatively, create a temporary object, assign the name,
                // then stuff it in the vector
                // object_ o = item.second;
                // o.name = item.first;
                // children.push_back(o);

                // Or, use a nonconst iterator
//                item.second.name = item.first;
//                children.push_back(item.second);
                
                debug(std::hex << this << ": Add child " << item.first << " = " << &item.second << "\n");
            }
                
        }

        basic_object(const double_type &double_value) : basic_object() {
            debug("double constructor invoked " << std::hex << this << "\n");
            set_value(double_value);
        }
        
        basic_object(const int_type &int_value) : basic_object()  {
            debug("int(" << int_value << ") constructor invoked " << std::hex << this << "\n");
            set_value(int_value);
        }
        
        // note: this constructor will only be invoked when a std::string is
        // passed to this object. String literals (pointers) get coerced to bool
        basic_object(const string_type &string_value) : basic_object()  {
            debug("string constructor invoked " << std::hex << this << "\n");
            set_value(string_value);
        }
        
        basic_object(const char str[]) : basic_object()  {
            debug("const char[] constructor invoked " << std::hex << this << "\n");
            set_value(str);
        }
        
		basic_object(const bool &bool_value) : basic_object()  {
            debug("bool constructor invoked " << std::hex << this << "\n");
			set_value(bool_value);
		}
        
        /** Null constructor. Effectively the same as the default constructor, 
         but helpful when initializing child objects in an initializer list */
        basic_object(const std::nullptr_t &) : basic_object()  {
            debug("nullptr_t constructor invoked " << std::hex << this << "\n");
            object_kind = kind_null; 
        }
        
        // copy assignment operator
        basic_object &operator =(basic_object &other) {
            debug("copy assignment operator invoked " << std::hex << this << "\n");
            
            object_kind = other.object_kind;
            double_value = other.double_value;
            int_value = other.int_value;
            string_value = other.string_value;
            children.clear();
            children.insert(children.begin(), other.children.begin(), other.children.end());
            
            return *this;
        }

        // const copy assignment operator
        basic_object &operator =(const basic_object &other) {
            debug("const copy assignment operator invoked " << std::hex << this << "\n");
            
            object_kind = other.object_kind;
            double_value = other.double_value;
            int_value = other.int_value;
            string_value = other.string_value;
            children.clear();
            children.insert(children.begin(), other.children.begin(), other.children.end());
            
            return *this;
        }
        
        // move assignment
        basic_object& operator =(basic_object &&other) {
            debug("move assignment operator (object) invoked " << std::hex << this << "\n");

            name = std::move(other.name);
            object_kind = other.object_kind;
            double_value = other.double_value;
            int_value = other.int_value;
            string_value = std::move(other.string_value);
            children = std::move(other.children);
            
            other.object_kind = kind_invalid;
            return *this;
        }
		
        /** invalid object test operator */
        bool operator !() const {
            return !is_valid();
        }
        
        /** Equality comparisons directly to intrinsic types */
        bool operator ==(const double_type &other) const {
            return object_kind == kind_number &&
                   double_value == other;
        }
        
        bool operator ==(const int_type &other) const {
            return object_kind == kind_number &&
                   int_value == other;
        }
        
        bool operator ==(const string_type &other) const {
            return object_kind == kind_string &&
                   string_value.compare(other) == 0;
        }
        
        bool operator ==(const char other[]) const {
            return object_kind == kind_string &&
                   string_value.compare(other) == 0;
        }
        
        bool operator ==(const bool &other) const {
            return (is_true() && other) ||
            (is_false() && !other);
        }
        
        bool operator ==(const std::nullptr_t &other) const {
            return is_null();
        }

		/** ne comparisons */
		bool operator !=(const double_type &other) const {
			return !(*this == other);
		}
        
        bool operator !=(const int_type &other) const {
            return !(*this == other);
        }
        
        bool operator !=(const char other[]) const {
            return !(*this == other);
        }

        bool operator !=(const bool &other) const {
            return !(*this == other);
        }

        bool operator !=(const string_type &other) const {
            return !(*this == other);
        }
        
        bool operator !=(const std::nullptr_t &other) const {
            return !(*this == other);
        }

        /** relational comparisons */ 
        bool operator >(const double_type &other) const {
            return object_kind == kind_number &&
                   double_value > other;
        }
        
        bool operator >(const int_type &other) const {
            return object_kind == kind_number &&
                   int_value > other;
        }

        bool operator >=(const double_type &other) const {
            return (*this > other) || (*this == other);
        }
        
        bool operator >=(const int_type &other) const {
            return (*this > other) || (*this == other);
        }

        bool operator <(const double_type &other) const {
            return object_kind == kind_number &&
                   double_value < other;
        }
        
        bool operator <(const int_type &other) const {
            return object_kind == kind_number &&
                   int_value < other;
        }

        bool operator <=(const double_type &other) const {
            return (*this < other) || (*this == other);
        }
        
        bool operator <=(const int_type &other) const {
            return (*this < other) || (*this == other);
        }
        
        bool is_valid() const {
            return object_kind != kind_invalid;
        }
        
        bool is_false() const {
            return object_kind == kind_false;
        }
        
        bool is_true() const {
            return object_kind == kind_true;
        }
        
        bool is_null() const {
            return object_kind == kind_null;
        }
        
        bool is_number() const {
            return object_kind == kind_number;
        }
        
        bool is_string() const {
            return object_kind == kind_string;
        }
        
        bool is_array() const {
            return object_kind == kind_array;
        }
        
        bool is_object() const {
            return object_kind == kind_object;
        }
        
        bool get_value(double_type &value) const {
            if (is_number()) {
                value = double_value;
                return true;
            }
            
            return false;
        }
    
        bool get_value(int_type &value) const {
            if (is_number()) {
                value = int_value;
                return true;
            }
            
            return false;
        }
    
        bool get_value(string_type &value) const {
            if (is_string()) {
                value = string_value;
                return true;
            }
            
            return false;
        }
        
        void set_value(const double_type &value) {
            object_kind = kind_number;
            double_value = value;
            int_value = static_cast<int_type>(value);
        }
        
        void set_value(const int_type &value) {
            object_kind = kind_number;
            int_value = value;
            double_value = static_cast<double_type>(value);
        }
        
        void set_value(const string_type &value) {
            object_kind = kind_string;
            string_value = value;
        }
		
        void set_value(const char value[]) {
            set_value(string_type(value));
        }
        
		void set_value(const bool value) {
			if (value) {
				object_kind = kind_true;
			}
			else {
				object_kind = kind_false;
			}
		}
        
        void set_value(const std::nullptr_t nothing) {
            object_kind = kind_null;
        }
        
        // child manipulation
//        void add_children(const vector<double_type> &vec_d) {}
//        void add_children(const vector<int_type> &vec_i) {}
//        void add_children(const vector<string_type> &vec_s) {}
//        void add_children(const basic_object<double_type, int_type, string_type> &o) {}

        
        /* Adding children: 
         There are two approaches here. 
         1 - Adding a child implicitly converts the item to an object or array, 
             depending on whether the child has a name
         2 - Adding a child to something that's not an array or object generates
             an error, in other words the user has to be explicit about what 
             they're doing. 

         Not sure which is better at the moment. However, the usage patterns 
         that have been developing seem to favor #1. 
         
         For example:
            object o;                   // create new (null) item
            o.add_child("value", 5);    // object "becomes" an object
         
            object o2; 
            o = 5;                      // object "becomes" an int value
         
         The type name is, in fact, "object", so it's likely the user can assume
         that it is an object and children can be added to it. 
         
         Explicit syntax: 
            object o = create_object(); // explicitly create object with helper function
            o.add_child("value", 5);
         
         or
         
            object o; 
            o.set_kind(kind_object); 
            o.add_child("value", 5); 
            
         This syntax seems cumbersome, which seems to favor the implicity
         However, the odd part comes when someone does this: 
         
            object o(1.23);             // explicitly create a double
            // ...
            o.add_child("value", 5);    // implicitly becomes an object
        
         There's an additional problem, in that object_'s know if they have names ...
         Thus, the following actually yields a valid object:
         
            object o1(1);
            o1.name = "one";
            object o2(1);
            o2.name = "one";
         
            object container{o1, o2};
            // container is an array
         
            container.object_kind = kind_object
            // container is now an *object*, and it's items have names
         
         One solution is to provide an interface to object_kind, i.e., 
            void change_kind(kind new_kind) {
                object_kind = new_kind; 
                if (object_kind != kind_array && object_kind != kind_object) {
                    // this object can't have children
                    children.erase();
                }
            }
         */
        
        /** Add named child to object */
        void add_child(const string_type &name, object_ child) {
            debug("add_child to object [implicit]\n");
            
            // implicitly become an object
            object_kind = kind_object;
            
            child.name = name;
            children.push_back(child);
        }
        
        /** Add child to array */
        void add_child(object_ child) {
            debug("add_child to array [implicit]\n");
            children.push_back(child);
        }
        
        /** Add child to array, throw an exception if not an object */
        void add_child_explicit(const string_type &name, object_ child) {
            if (object_kind != kind_object) {
                throw std::runtime_error("Tried to add a child to non-object");
            }
            
            debug("add_child to object [explicit]\n");
            child.name = name;
            children.push_back(child);
        }
        
        /** Add child to array, throw an exception if not an array */
        void add_child_explicit(object_ &child) {
            if (object_kind != kind_array) {
                throw std::runtime_error("Tried to add a child to non-array");
            }

            debug("add_child to array [explicit]\n");
            children.push_back(child);
        }
        
//        void add_children(const vector<pair<string_type, const basic_object<double_type, int_type, string_type>>> &children) {}
//        void add_children(const vector<pair<string_type, const double_type>> &children) {}
//        void add_children(const vector<pair<string_type, const int_type>> &children) {}
//        void add_children(const vector<pair<string_type, const string_type>> &children) {}
        
        // Return a _reference_ to a child object
        // map version
//        const object_reference_ get_child(const string_type name) {
//            object_data_iterator i = children.find(name);
//            
//            if (i == children.end()) {
//                return object_reference_(nullptr);
//            }
//            else {
//                return object_reference_(&i->second);
//            }
//        }
        const object_reference_ get_child(const string_type name) {
            object_data_iterator i = std::find_if(children.begin(), children.end(),
                [&name] (const object_& a) {
                    return a.name == name;
                });
            
            if (i == children.end()) {
                return object_reference_(nullptr);
            }
            else {
                return object_reference_(&(*i));
            }
        }
        
		/** Return a C++ reference to a child object. Throw a runtime error if 
			the child doesn't exist */
        // map version
//		object_ &get_child2(const string_type name) {
//            object_data_iterator i = children.find(name);
//            
//            if (i == children.end()) {
//				throw std::runtime_error("No such child");
//            }
//            else {
//                return i->second;
//            }
//		}
		object_ &get_child2(const string_type &name) {
            object_data_iterator i = std::find_if(children.begin(), children.end(),
                                                  [&name] (const object_& a) {
                                                      return a.name == name;
                                                  });
            
            if (i == children.end()) {
				throw std::runtime_error("No such child");
            }
            else {
                return *i;
            }
		}
		
        /** Return a C++ reference to a child object. Throw a runtime error if
         the child doesn't exist or if this isn't an indexable object (not an 
         array).
         Array version */
        object_ &get_child_index(uint32_t index) {
            if (object_kind != kind_array) {
                throw std::runtime_error("Tried to index non-array");
            }
            
            if (index >= children.size()) {
                throw std::runtime_error("Index out of range");
            }
            
            return children.at(index);
        }
        
		/** Test if the object contains a child with the specified name */
		bool has_child(const string_type name) const {
            object_data_const_iterator i =
                std::find_if(children.begin(), children.end(),
                    [&name] (const object_& a) {
                      return a.name == name;
                    });

            return i != children.end();
		}
		
        // Same as get_child, except you get the actual object
        object_ detach_child(const string_type name) {
            object_data_iterator i = std::find_if(children.begin(), children.end(),
                                                  [&name] (const object_& a) {
                                                      return a.name == name;
                                                  });
            
            if (i == children.end()) {
				return invalid_object();
            }

            object_ child = *i;
            children.erase(i);
            return child;
        }
        
		// Steal a child from another object
		bool transfer_child(object_ &o, string_type name) {
			object_ child = o.detach_child(name);
			if (!child) {
				return false; 
			}
			
			add_child(name, child); 
			return true;
		}
        
        string_type to_string() const {
            switch (object_kind) {
                case kind_null:
                    return "null";
                    break;
                case kind_false:
                    return "false";
                    break;
                case kind_true:
                    return "true";
                    break;
                case kind_number:
                    if (static_cast<double_type>(int_value) == double_value) {
                        return std::to_string(int_value);
                    }
                    else {
                        return std::to_string(double_value);
                    }
                    break;
                case kind_string:
                    return "\"" + jsonex::util::utf8_to_json(string_value) + "\"";
                    break;
                case kind_array:
                    {
                    vector<string> values;
					values.reserve(children.size());
                    for (auto child: children) {
                        values.push_back(child.to_string());
                    }
                    return "[" + jsonex::util::join(values, ",") + "]";
                    }
                    break;
                case kind_object:
                    {
                        vector<string> key_values;
                    	key_values.reserve(children.size());
                        for (auto child: children) {
                            key_values.push_back("\"" + child.name + "\":" + child.to_string());
                        }
                    
                        return "{" + jsonex::util::join(key_values, ",") + "}";
                    }
                    break;
            }

            return "";

        }
        
        // TODO: Print out a human-readble object
        string_type to_pretty_string() const {
            return "";
        }
        
    };
    

    
}

void test_array_initializer_list();

#endif