//
//  jsonex_parser.h
//  jsonex
//
//  Created by Seth Hill on 3/7/14.
//  Copyright (c) 2014 Seth Hill. All rights reserved.
//

#ifndef __jsonex__jsonex_parser__
#define __jsonex__jsonex_parser__

#include <stack>
#include <string>
#include "jsonex.hpp"

namespace jsonex {
    using std::string;

    // This version of the parser uses the default object type
    using jsonex::object;
    
    /** create an object from a string of json, as defined on json.org
     */
    
    class recursive_parser {
    public:
        
        /** Eat whitespace. Advance start iterator past the whitespace.
         Returns false if we run into the end, true otherwise. */
        static bool __consume_whitespace(string::const_iterator &start,
                                         string::const_iterator &end)
        {
            debug("Enter __consume_whitespace with '" << std::string(start, start + 40) << "...'\n");
            
            for (; start != end; start++) {
                if (!std::isspace(*start)) {
                    return true;
                }
            }
            
            return false;
        }

        /** Find a token, optionally surrounded by whitespace.
         Advance start iterator past the whitespace that follows the token, if 
         found.
         Returns false if we run into the end, true otherwise. */
        static bool __consume_token(const char token,
                                    string::const_iterator &start,
                                    string::const_iterator &end)
        {
            debug("Enter __consume_token with '" << std::string(start, start + 40) << "...'\n");

            string::const_iterator &iter = start;
            
            if (!__consume_whitespace(iter, end)) {
                return false;
            }
            
            if (*iter == token) {
                iter++;
            }
            else {
                return false;
            }

            if (!__consume_whitespace(iter, end)) {
                return false;
            }
            
            start = iter;
            
            return true;
        }

        static bool __parse_object(string::const_iterator &start,
                                   string::const_iterator &end,
                                   vector <object>& out)
        {
            debug("Enter __parse_object with '" << std::string(start, start + 40) << "...'\n");
            
            string::const_iterator iter = start;
            object temp;
            string key;
            
            // not the beginning of an object
            if (!__consume_token('{', iter, end)) {
                return false;
            }

            while (iter != end) {
                
                if (*iter == '}') {
                    // consume the '}' and return
                    std::advance(start, (iter + 1) - start);
                    return true;
                }
                
                // find the key
                if (*iter != '\"') {
                    debug("Malformed object at " << (iter - start) << "\n");
                    return false;
                }
                
                if (!__parse_string(iter, end, key)) {
                    debug("Malformed object at " << (iter - start) << "\n");
                    return false;
                }
                
                if (!__consume_token(':', iter, end)) {
                    // whitespace to end of string, failed
                    debug("__parse_object: Encountered end of string while looking for ':'\n");
                    return false;
                }
                                
                if (__parse(iter, end, temp)) {
                    temp.name = key;
                    out.push_back(temp);
                }
                else {
                    debug("invalid json at '" << std::string(iter, iter + 40) << "'\n");
                    return false;
                }
                
                if (!__consume_whitespace(iter, end)) {
                    debug("__parse_object: Encountered end of string while looking for ',' or '}'\n");
                    return false;
                }
                
                if (*iter == ',') {
                    iter++; 
                    if (!__consume_whitespace(iter, end)) {
                        // whitespace to end of string, failed
                        debug("__parse_object: Encountered end of string while looking for object members\n");
                        return false;
                    }
                }
                // else, it could be a '}', check on next loop iteration
            }
            
            return false;
        }

        // Make sure there are at least the same number of ]'s in the input
        // string as ['s
        // A weak attempt at defense against a kind of malicious data
/*        static bool __find_balance(string::const_iterator &start,
                                   string::const_iterator &end) {
            static int entry_count = 0;
            cout << entry_count++ << "\n";
            int lefts = 0;
            int rights = 0;
            
            string::const_iterator iter = start;
            for (; iter != end; iter++) {
                if (*iter == '[') {
                    lefts++;
                }
                else if (*iter == ']') {
                    rights++;
                }
            }
            
            return lefts > 0 && rights >= lefts;
        }*/
        
        static bool __parse_array(string::const_iterator &start,
                                  string::const_iterator &end,
                                  vector<object> &out)
        {
            debug("Enter __parse_array with '" << std::string(start, start + 40) << "...'\n");

           /* if (!__find_balance(start, end)) {
                return false;
            }*/
            
            string::const_iterator iter = start;
            
            object temp;
            
            // not the beginning of an array
            if (*iter++ != '[') {
                return false;
            }
            
            // continue looking for objects separated with ',' until we hit
            // the end of the array or the end of the string
            while (iter != end) {
                
                if (!__consume_whitespace(iter, end)) {
                    return false;
                }
                
                if (*iter == ']') {
                    // consume the ] and return
                    std::advance(start, (iter + 1) - start);
                    return true;
                }
                
                if (__parse(iter, end, temp)) {
                    out.push_back(temp);
                }
                else {
                    iter = end;
                    debug("invalid json at '" << std::string(iter, iter + 40) << "'\n");
                    return false;
                }
                
                if (!__consume_whitespace(iter, end)) {
                    debug("__parse_object: Encountered end of string while looking for ',' or '}'\n");
                    return false;
                }
                
                if (*iter == ',') {
                    iter++;
                    if (!__consume_whitespace(iter, end)) {
                        // whitespace to end of string, failed
                        debug("__parse_object: Encountered end of string while looking for array members\n");
                        return false;
                    }
                }
                // else, it could be a ']', check on next loop iteration
            }
            
            return false;
        }

        static bool __parse_number(string::const_iterator &start,
                                   string::const_iterator &end, object& out)
        {
            debug("Enter __parse_number with '" << std::string(start, start + 40) << "...'\n");

            string::const_iterator iter = __find_number(start, end);
            
            if (iter == start) {
                // not a number
                return false;
            }
            else {
                string num(start, iter);
                
                if (num.find('.') == string::npos &&
                    num.find('e') == string::npos &&
                    num.find('E') == string::npos) {
                    try {
                        out.set_value(std::stoi(num));
                    }
                    catch (std::out_of_range) {
                        return false;
                    }
                }
                else {
                    try {
                        out.set_value(std::stod(num));
                    }
                    catch (std::out_of_range) {
                        return false;
                    }
                }
                
                start = iter;
                return true;
            }
        }

        /** Find a JSON string, starting at start. 
         A string word characters surrounded by double quotes. 

         This function _will_ do backslash escaping, but not quite yet.
         */
        static bool __parse_string(string::const_iterator &start,
                                   string::const_iterator &end,
                                   std::string &out)
        {
            debug("Enter __parse_string with '" << std::string(start, start + 40) << "...'\n");

            if (*start != '\"') {
                // didn't find the opening double quote
                return false;
            }
            
            string::const_iterator iter = start + 1;
            // Found an empty string
            if (*iter == '\"') {
                out = "";
                start = iter + 1;
                return true;
            }

            iter = __find_word(iter, end);
            
            if (iter == start + 1) {
                // didn't find any words, or ran into the end of the input
                // string before we were finished
                return false;
            }
            
            if (iter == end) {
                // string found, but end of input string found before closing
                // double quote
                return false;
            }
            
            // __find_word returns 1 past the end of the word characters,
            // so if the string is good, iter points to the closing quote
            if (*iter != '\"') {
                // didn't find the closing double quote
                return false;
            }
            
            // create a string object, skipping the opening and closing quotes.
            out = string((start + 1), (iter));
            
            // advance start past the closing quote
            start = iter + 1;
            return true;
            
        }
        
        
        // Parse a string into an object
        static bool __parse_string(string::const_iterator &start,
                                   string::const_iterator &end, object& out)
        {
            std::string s;
            if (__parse_string(start, end, s)) {
                out.set_value(jsonex::util::json_to_utf8(s));
                return true;
            }
            
            return false;
        }
        
        
        /** Search for a JSON word, starting at start. Return an iterator that
         points to the last consecutive string character from start.
         
         A word is any unicode character except control chars, a backslash, and
         a double quote. Backslashes may be followed immediately by ", \,
         b (backspace), f (formfeed), n (new line), r (carriage return),
         t (horizontal tab), or a u followed by 4 hex digits (unicode escape).
         */

        static string::const_iterator __find_word(string::const_iterator &start,
                                                  string::const_iterator &end) {
            string::const_iterator iter = start;
            
            while (iter != end) {
                if (std::iscntrl(*iter)) {
                    // Note: if we return iter - 1, then we return an iterator
                    // that points to the last character, not after the last
                    // character, this means substr(start, iter) will be one
                    // char too short
                    return iter;
                }
                
                if (*iter == '\"') {
                    return iter;
                }
                
                // Escape?
                if (*iter == '\\') {
                    // Absurd condition to see if we have a unicode escape uXXXX
                    if (iter + 5 != end &&
                        *(iter + 1) == 'u' &&
                        std::isxdigit(*(iter + 2)) &&
                        std::isxdigit(*(iter + 3)) &&
                        std::isxdigit(*(iter + 4)) &&
                        std::isxdigit(*(iter + 5)))
                    {
                        iter += 5;
                        continue;
                    }
                    
                    // Check for the other valid escapes
                    if (iter + 1 != end &&
                        (*(iter + 1) == '\\' ||
                         *(iter + 1) == '/' ||
                         *(iter + 1) == 'b' ||
                         *(iter + 1) == 'f' ||
                         *(iter + 1) == 'n' ||
                         *(iter + 1) == 'r' ||
                         *(iter + 1) == 't'))
                    {
                        iter += 2;
                        continue;
                    }
                    
                    // else, end of word, invalid escape
                    return iter;
                }
                
                iter++;
            }
            
            // word ends at the end of the input string
            return iter;
        }
        
        /** Search for a JSON number, starting at start. Return an iterator that
         points to the last consecutive number character from start.
         
         A number is: 
         Optional negative (-)
         Either 0 or 1-9 followed by 0-9+
         Optional . followed by digits
         Optional e|E followed by optional +|- then digits
         */
        static string::const_iterator __find_number(string::const_iterator &start,
                                                    string::const_iterator &end)
        {
            string::const_iterator iter = start;
            
            // Leading negative?
            if (*iter == '-') {
                iter++;

                // a single - isn't a number
                if (iter == end) {
                    return start;
                }
            }
            
            // Single zero
            if (*iter == '0') {
                iter++;
                
                if (iter == end) {
                    // valid 0
                    return iter;
                }
            }
            // single 1-9 followed by 0-9+
            else if (*iter >= '1' && *iter <= '9') {
                ++iter;
                
                while ((iter != end) && (*iter >= '0' && *iter <= '9')) {
                    ++iter;
                }
                
                if (iter == end) {
                    // valid 1[0-9]+
                    return iter;
                }
            }
            
            // optional '.' followed by >= 1 digits
            if (*iter == '.') {
                ++iter;
                if (iter == end) {
                    // fail, at least one digit must follow a .
                    return start;
                }
                while (iter != end && (*iter >= '0' && *iter <= '9')) {
                    ++iter;
                }
                
                if (iter == end) {
                    // valid [0-9]+.[0-9]+
                    return iter;
                }
            }
            
            // optional e or E, optionally followed by + or -, followed by digits
            if (*iter == 'e' || *iter == 'E') {
                ++iter;
                
                if (iter == end) {
                    // fail, nothing following e
                    return start;
                }

                if (*iter == '+' || *iter == '-') {
                    ++iter;

                    if (iter == end) {
                        // fail, nothing following e±
                        return start;
                    }
                }

                while (iter != end && (*iter >= '0' && *iter <= '9')) {
                    ++iter;
                }
            }
            
            return iter;
        }
        
        static bool __parse_null_or_bool(string::const_iterator &start,
                                         string::const_iterator &end,
                                         object& out)
        {
            debug("Enter __parse_null_or_bool with '" << std::string(start, start + 40) << "'...\n");

            string::const_iterator word_end = start + 4;
            string sub(start, word_end);
            
            if (sub == "true") {
                out = object(true);
            }
            
            else if (sub == "null") {
                out = object(nullptr);
            }
            
            else {
                word_end = start + 5;
                sub = string(start, word_end);
                
                if (sub == "false") {
                    out = object(false);
                }
                else {
                    return false;
                }
                    
            }
            
            /* in addition to finding true, false, or bool, we also have to 
             make sure the following character is either the end of string, 
             a space, a comma, a ], or a } */
            
            if (word_end != end &&
                !std::isspace(*word_end) &&
                *word_end != ',' &&
                *word_end != ']' &&
                *word_end != '}')
            {
                return false;
            }
           
            std::advance(start, word_end - start);
            return true;
        }
    
        /** Find a single JSON value. 
         On success, return true, advance the start iterator to immediately 
         after the value, return the jsonex::object in out.
         On failure, return false, don't move iterators and don't touch out. 
         Hint: this could be used to parse a series of whitespace-separated
         JSON objects...
         */
        static bool __parse(string::const_iterator &iter,
                            string::const_iterator &end, object& out)
        {
            object result, temp;
            vector<object> temp_list;
            
            // eat some whitespace
            if (!__consume_whitespace(iter, end)) {
                debug("__parse: encountered end of string while looking for value\n");
                return false;
            }
            
            // Note: determining if something is an object, array, or string
            // is fast (check initial character for '{', '[', or '"'
            // Searching for numbers is slightly slower
            // Searching for true/false/null is slowest, because we have to
            // do string comparisons
            // Searches are ordered appropriately.
            
            // Object?
            if (__parse_object(iter, end, temp_list)) {
                debug(".found object\n");
                out.object_kind = kind_object;
                // important to clear the vector here, otherwise we'll add too
                // many children
                out.children = std::move(temp_list);
                return true;
            }
            
            else if (__parse_array(iter, end, temp_list)) {
                debug(".found array\n");
                out = jsonex::array(temp_list);
                return true;
            }
            
            else if (__parse_string(iter, end, temp)) {
                debug(".found string\n");
                out = std::move(temp);
                return true;
            }
            
            else if (__parse_number(iter, end, temp)) {
                debug(".found number\n");
                out = std::move(temp);
                return true;
            }
            
            else if (__parse_null_or_bool(iter, end, temp)) {
                debug(".found null or bool\n");
                out = std::move(temp);
                return true;
            }
            
            return false;
        }
    
        static object from_string(const string &json) {
            debug("String: '" << json << "'\n");
            
            jsonex::object result;
            
            string::const_iterator begin = json.cbegin();
            string::const_iterator end = json.cend();
            
            if (__parse(begin, end, result)) {
                // success
                debug("Parse succeeded, remaining data = '" << string(begin, end) << "'\n");
                return result;
            }
            else {
                // failure
                debug("Parse failed, remaining data = '" << string(begin, end) << "'\n");
                return jsonex::object::invalid_object();
            }
            
            return result;
        }
    };
    
    using std::stack;
    using std::make_pair;
    

    class stack_parser {
    public:
        enum parse_mode { none, obj, arr };
        
        struct parser_state {
            parse_mode state;
            object& parent;
            string::const_iterator start;
            string::const_iterator end;
        };
        typedef std::stack<parser_state> parse_stack;
        
        static object from_string(const string &json) {
            debug("String: '" << json << "'\n");
            
            jsonex::object result;
            string::const_iterator begin = json.cbegin();
            string::const_iterator end = json.cend();
            
            if (__parse(begin, end, result)) {
                // success
                debug("Parse succeeded, remaining data = '" << string(begin, end) << "'\n");
                return result;
            }
            else {
                // failure
                debug("Parse failed, remaining data = '" << string(begin, end) << "'\n");
                return jsonex::object::invalid_object();
            }
            
            return result;
        }
        
        static bool __parse(string::const_iterator &start,
                            string::const_iterator &end, object& out)
        {
            parse_stack st;
            
            string::const_iterator &iter = start;

            // get the stack ready
            st.push({none, out, iter, end});
            
            while (!st.empty()) {
                // cout << "Stack: " << st.size() << " Object: " << out.to_string() << "\n";
                parser_state &info = st.top();
                parse_mode state = info.state;
                jsonex::object& parent = info.parent;
                jsonex::object temp;
                if (state == none) {
                    st.pop();
                }

                // eat some whitespace
                if (!recursive_parser::__consume_whitespace(iter, end)) {
                    debug("__parse: encountered end of string while looking for value\n");
                    return false;
                }
            
                if (*iter == ']') {
                    if (state == arr) {
                        st.pop();
                        iter++;
                        continue;
                    }
                    else {
                        debug("__parse: encountered unexpected ']' at " << (iter - start) << "\n");
                        return false;
                    }
                }

                if (*iter == '}') {
                    if (state == obj) {
                        st.pop();
                        iter++;
                        continue;
                    }
                    else {
                        debug("__parse: encountered unexpected ']' at " << (iter - start) << "\n");
                        return false;
                    }
                }
                
                
                if (state == obj || state == arr) {
                    if (!__consume_token(',', iter, end)) {}
                }
                
                if (state == obj) {
                    // find the key
                    if (!recursive_parser::__parse_string(iter, end, temp.name)) {
                        debug("Malformed object at " << (iter - start) << "\n");
                        return false;
                    }
                    
                    if (!__consume_token(':', iter, end)) {
                        // whitespace to end of string, failed
                        debug("__parse_object: Encountered end of string while looking for ':'\n");
                        return false;
                    }
                }
                
                if (0) {}
                
                else if (*iter == '{') {
                    ++iter;
                    
                    // Found an object, add it to the parent
                    if (state == obj) {
                        parent.add_child(temp.name, jsonex::object::create_object());
                        if (__consume_token('}', iter, end)) {
//                            __consume_token(',', iter, end);
//                            continue;
                        }
                        else {
                            st.push({obj, parent.children.back(), iter, end});
                        }
                    }
                    else if (state == arr) {
                        parent.add_child(jsonex::object::create_object());
                        if (__consume_token('}', iter, end)) {
//                            __consume_token(',', iter, end);
//                            continue;
                        }
                        else {
                            st.push({obj, parent.children.back(), iter, end});
                        }
                    }
                    else {
                        parent.object_kind = kind_object;
                        if (__consume_token('}', iter, end)) {
                            continue;
                        }
                        st.push({obj, parent, iter, end});
                    }
                    
                    continue;
                }
                
                else if (*iter == '[') {
                    ++iter;

                    if (state == obj) {
                        parent.add_child(temp.name, jsonex::object::create_array());
                        if (__consume_token(']', iter, end)) {
//                            __consume_token(',', iter, end);
//                            continue;
                        }
                        else {
                            st.push({arr, parent.children.back(), iter, end});
                        }
                    }
                    else if (state == arr) {
                        parent.add_child(jsonex::object::create_array());
                        if (__consume_token(']', iter, end)) {
//                            __consume_token(',', iter, end);
//                            continue;
                        }
                        else {
                            st.push({arr, parent.children.back(), iter, end});
                        }
                    }
                    else {
                        parent.object_kind = kind_array;
                        if (__consume_token(']', iter, end)) {
                            continue;
                        }
                        st.push({arr, parent, iter, end});
                    }
                }
                
                else if (__parse_number(iter, end, temp) ||
                         __parse_string(iter, end, temp) ||
                         __parse_null_or_bool(iter, end, temp))
                {
                    if (state == obj) {
                        parent.add_child(temp.name, temp);
                    }
                    else if (state == arr) {
                        parent.add_child(temp);
                    }
                    else {
                        parent = temp;
                    }
                }
                
                else {
                    // Not a new object, not a new array, not a number, bool,
                    // string, or null, not the end of an array or object.
                    // i.e., invalid syntax
                    debug("__parse: Invalid syntax");

                    return false;
                }
            }
            
            return true;
        }

        
        static bool __parse_number(string::const_iterator &start,
                                   string::const_iterator &end, object& out)
        {
            debug("Enter __parse_number with '" << std::string(start, start + 40) << "...'\n");
            
            string::const_iterator iter = __find_number(start, end);
            
            if (iter == start) {
                // not a number
                return false;
            }
            else {
                string num(start, iter);
                
                if (num.find('.') == string::npos &&
                    num.find('e') == string::npos &&
                    num.find('E') == string::npos) {
                    try {
                        out.set_value(std::stoi(num));
                    }
                    catch (std::out_of_range) {
                        return false;
                    }
                }
                else {
                    try {
                        out.set_value(std::stod(num));
                    }
                    catch (std::out_of_range) {
                        return false;
                    }
                }
                
                start = iter;
                return true;
            }
        }
        
        /** Find a JSON string, starting at start.
         A string word characters surrounded by double quotes.
         
         This function _will_ do backslash escaping, but not quite yet.
         */
        static bool __parse_string(string::const_iterator &start,
                                   string::const_iterator &end,
                                   std::string &out)
        {
            debug("Enter __parse_string with '" << std::string(start, start + 40) << "...'\n");
            
            if (*start != '\"') {
                // didn't find the opening double quote
                return false;
            }
            
            string::const_iterator iter = start + 1;
            // Found an empty string
            if (*iter == '\"') {
                out = "";
                start = iter + 1;
                return true;
            }
            
            iter = __find_word(iter, end);
            
            if (iter == start + 1) {
                // didn't find any words, or ran into the end of the input
                // string before we were finished
                return false;
            }
            
            if (iter == end) {
                // string found, but end of input string found before closing
                // double quote
                return false;
            }
            
            // __find_word returns 1 past the end of the word characters,
            // so if the string is good, iter points to the closing quote
            if (*iter != '\"') {
                // didn't find the closing double quote
                return false;
            }
            
            // create a string object, skipping the opening and closing quotes.
            out = string((start + 1), (iter));
            
            // advance start past the closing quote
            start = iter + 1;
            return true;
            
        }
        
        
        // Parse a string into an object
        static bool __parse_string(string::const_iterator &start,
                                   string::const_iterator &end, object& out)
        {
            std::string s;
            if (__parse_string(start, end, s)) {
                out.set_value(jsonex::util::json_to_utf8(s));
                return true;
            }
            
            return false;
        }
        
        
        /** Search for a JSON word, starting at start. Return an iterator that
         points to the last consecutive string character from start.
         
         A word is any unicode character except control chars, a backslash, and
         a double quote. Backslashes may be followed immediately by ", \,
         b (backspace), f (formfeed), n (new line), r (carriage return),
         t (horizontal tab), or a u followed by 4 hex digits (unicode escape).
         */
        
        static string::const_iterator __find_word(string::const_iterator &start,
                                                  string::const_iterator &end) {
            string::const_iterator iter = start;
            
            while (iter != end) {
                if (std::iscntrl(*iter)) {
                    // Note: if we return iter - 1, then we return an iterator
                    // that points to the last character, not after the last
                    // character, this means substr(start, iter) will be one
                    // char too short
                    return iter;
                }
                
                if (*iter == '\"') {
                    return iter;
                }
                
                // Escape?
                if (*iter == '\\') {
                    // Absurd condition to see if we have a unicode escape uXXXX
                    if (iter + 5 != end &&
                        *(iter + 1) == 'u' &&
                        std::isxdigit(*(iter + 2)) &&
                        std::isxdigit(*(iter + 3)) &&
                        std::isxdigit(*(iter + 4)) &&
                        std::isxdigit(*(iter + 5)))
                    {
                        iter += 5;
                        continue;
                    }
                    
                    // Check for the other valid escapes
                    if (iter + 1 != end &&
                        (*(iter + 1) == '\\' ||
                         *(iter + 1) == '/' ||
                         *(iter + 1) == 'b' ||
                         *(iter + 1) == 'f' ||
                         *(iter + 1) == 'n' ||
                         *(iter + 1) == 'r' ||
                         *(iter + 1) == 't'))
                    {
                        iter += 2;
                        continue;
                    }
                    
                    // else, end of word, invalid escape
                    return iter;
                }
                
                iter++;
            }
            
            // word ends at the end of the input string
            return iter;
        }
        
        /** Search for a JSON number, starting at start. Return an iterator that
         points to the last consecutive number character from start.
         
         A number is:
         Optional negative (-)
         Either 0 or 1-9 followed by 0-9+
         Optional . followed by digits
         Optional e|E followed by optional +|- then digits
         */
        static string::const_iterator __find_number(string::const_iterator &start,
                                                    string::const_iterator &end)
        {
            string::const_iterator iter = start;
            
            // Leading negative?
            if (*iter == '-') {
                iter++;
                
                // a single - isn't a number
                if (iter == end) {
                    return start;
                }
            }
            
            // Single zero
            if (*iter == '0') {
                iter++;
                
                if (iter == end) {
                    // valid 0
                    return iter;
                }
                
                if (*iter != '.' || *iter != 'e' || *iter != 'E') {
                    // single zeros must be followed with [.eE]
                    return start;
                }
            }
            // single 1-9 followed by 0-9+
            else if (*iter >= '1' && *iter <= '9') {
                ++iter;
                
                while ((iter != end) && (*iter >= '0' && *iter <= '9')) {
                    ++iter;
                }
                
                if (iter == end) {
                    // valid 1[0-9]+
                    return iter;
                }
            }
            
            // optional '.' followed by >= 1 digits
            if (*iter == '.') {
                ++iter;
                if (iter == end) {
                    // fail, at least one digit must follow a .
                    return start;
                }
                while (iter != end && (*iter >= '0' && *iter <= '9')) {
                    ++iter;
                }
                
                if (iter == end) {
                    // valid [0-9]+.[0-9]+
                    return iter;
                }
            }
            
            // optional e or E, optionally followed by + or -, followed by digits
            if (*iter == 'e' || *iter == 'E') {
                ++iter;
                
                if (iter == end) {
                    // fail, nothing following e
                    return start;
                }
                
                if (*iter == '+' || *iter == '-') {
                    ++iter;
                    
                    if (iter == end) {
                        // fail, nothing following e±
                        return start;
                    }
                }
                
                while (iter != end && (*iter >= '0' && *iter <= '9')) {
                    ++iter;
                }
            }
            
            return iter;
        }
        
        static bool __parse_null_or_bool(string::const_iterator &start,
                                         string::const_iterator &end,
                                         object& out)
        {
            debug("Enter __parse_null_or_bool with '" << std::string(start, start + 40) << "'...\n");
            
            string::const_iterator word_end = start + 4;
            string sub(start, word_end);
            
            if (sub == "true") {
                out.set_value(true);
            }
            
            else if (sub == "null") {
                out.set_value(nullptr);
            }
            
            else {
                word_end = start + 5;
                sub = string(start, word_end);
                
                if (sub == "false") {
                    out.set_value(false);
                }
                else {
                    return false;
                }
                
            }
            
            /* in addition to finding true, false, or bool, we also have to
             make sure the following character is either the end of string,
             a space, a comma, a ], or a } */
            
            if (word_end != end &&
                !std::isspace(*word_end) &&
                *word_end != ',' &&
                *word_end != ']' &&
                *word_end != '}')
            {
                return false;
            }
            
            std::advance(start, word_end - start);
            return true;
        }
        
        /** Find a token, optionally surrounded by whitespace.
         Advance start iterator past the whitespace that follows the token, if
         found.
         Returns true the token was found, false otherwise. */
        static bool __consume_token(const char token,
                                    string::const_iterator &start,
                                    string::const_iterator &end)
        {
            debug("Enter __consume_token(" << token << ") with '" << std::string(start, start + 40) << "...'\n");
            
            string::const_iterator &iter = start;
            
            if (!recursive_parser::__consume_whitespace(iter, end)) {
                return false;
            }
            
            if (*iter == token) {
                iter++;
            }
            else {
                return false;
            }
            
            // eat whitespace following token
            recursive_parser::__consume_whitespace(iter, end);
            
            start = iter;
            
            return true;
        }
    };

	typedef stack_parser parser;
}

#endif /* defined(__jsonex__jsonex_parser__) */
