//
//  jsonex_tests.m
//  jsonex_tests
//
//  Created by Seth Hill on 3/4/14.
//  Copyright (c) 2014 Seth Hill. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "jsonex.hpp"


@interface jsonex_tests : XCTestCase

@end

@implementation jsonex_tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


jsonex::object move_test() {
    jsonex::object o(5);
    
    return o;
}


- (void)testConstructors
{
    jsonex::object::double_type d;
    jsonex::object::int_type i;
    jsonex::object::string_type s;

    cout << "-- const obj\n";
    const jsonex::object const_obj(1);
    
    jsonex::object q;
    cout << "-- const assignment\n";
    q = const_obj;
    
    jsonex::object o;
    XCTAssert(o.is_null(), "Object isn't null.");
    
   
    jsonex::object od(1.0);
    XCTAssert(od.is_number(), "Object isn't a number.");
    od.get_value(d);
    od.get_value(i);
    XCTAssertEqual(d, 1.0, "Double value incorrect");
    XCTAssertEqual(i, 1, "Int value incorrect");
    
    jsonex::object oi(1);
    XCTAssert(oi.is_number(), "Object isn't a number.");
    oi.get_value(d);
    oi.get_value(i);
    XCTAssertEqual(d, 1.0, "Double value incorrect");
    XCTAssertEqual(i, 1, "Int value incorrect");
    
    jsonex::object obf(false);
    XCTAssert(obf.is_false(), "Object isn't false");
    
    jsonex::object obt(true);
    XCTAssert(obt.is_true(), "Object isn't true");
    
    jsonex::object moved = move_test();
}

-(void) test_array_ctor {
    jsonex::object arr = jsonex::array{1, 2};
   
    XCTAssertTrue(arr.children.size() == 2, "Array constructor failed");
}

-(void) test_initializer_list_ctor
{
    jsonex::object initializer_constructed{{"A", "B"}, {"C", "D"}};
    jsonex::object_reference ref = initializer_constructed.get_child("A");
    XCTAssertTrue(ref, "Reference not initialized");
    XCTAssertTrue(*ref == "B", "Reference value incorrect");
    
    ref = initializer_constructed.get_child("C");
    XCTAssertTrue(ref, "Reference not initialized");
    XCTAssertTrue(*ref == "D", "Reference value incorrect");
}

-(void) test_print_array {
    jsonex::object o = jsonex::array{{{"A", 1}}, {{"B", 2}}};
    
    cout << o.to_string() << "\n";
    
    jsonex::object o2 = jsonex::array{1, 2, 3, 4, "a", false, nullptr, "b", {{"Quasi", 7.334}}};
    cout << o2.to_string() << "\n";
}

-(void) test_complex_initializer_list_ctor
{
    jsonex::object o{
        {"A", {{"A1", true}, {"A2", false} }},
        {"B", {{"B1", nullptr}, {"B2", 54.32124} }},
        {"C", {{"C1", {{"C11", "11"}, {"C12", 12} }}, {"C2", 2} }},
    };
    
    jsonex::object o2;
    
    jsonex::object_reference ref = o.get_child("A");
    XCTAssertTrue(ref, "Reference not initialized");
    XCTAssertTrue(ref->is_object(), "Reference value not object");
    
    o2 = ref->get_child2("A1");
    XCTAssertTrue(o2.is_true(), "Child object not properly encoded");
    o2 = ref->get_child2("A2");
    XCTAssertTrue(o2.is_false(), "Child object not properly encoded");
    
    o2 = o.get_child2("B").get_child2("B1");
    XCTAssertTrue(o2.is_null(), "Child object not properly encoded");
    o2 = o.get_child2("B").get_child2("B2");
    XCTAssertEqual(o2.double_value, 54.32124, "Child object not properly encoded");
    
    o2 = o.get_child2("C").get_child2("C1").get_child2("C11");
    XCTAssertTrue(o2 == "11", "Sub sub child object not properly encoded");
    cout << "\n\n\n";
    cout << o.to_string();
    cout << "\n\n\n";
    cout << jsonex::object(2).to_string();
    cout << "\n\n\n";
}

- (void) test_string_ctor
{
    // string constructor
    std::string s1("Hello, world!");
    std::string s2;
    
    jsonex::object o(s1);
    XCTAssert(o.is_string(), "Object isn't a string.");
    
    o.get_value(s2);
    XCTAssertEqual(s2.compare(s1), 0, "String value incorrect");
}

- (void) test_const_char_p_ctor
{
    std::string s;
    jsonex::object o("Hello, world!");
    XCTAssert(o.is_string(), "Object isn't a string.");
    
    o.get_value(s);
    XCTAssertEqual(s.compare("Hello, world!"), 0, "String value incorrect");
}


- (void)test_move_ctor {
    
    std::vector<jsonex::object> ov;
    
    cout << "Adding 1\n";
    ov.push_back(jsonex::object("1"));
    
    cout << "Adding 2\n";
    ov.push_back(jsonex::object("2"));
    
    cout << "Adding 3\n";
    ov.push_back(jsonex::object(3));
    
    cout << "Adding 4\n";
    ov.push_back(jsonex::object(4.0));
    
    cout << "Adding 5\n";
    ov.push_back(5);
    
    cout << "Leaving\n";
}

-(void) test_copy_assignment
{
    std::vector<jsonex::object> ov{1, 2, 3, "A", false, nullptr};
    
    jsonex::object o;
    
    cout << "Get 0\n";
    o = ov.at(0);
    cout << "Get 1\n";
    o = ov.at(1);
    cout << "Get 2\n";
    o = ov.at(2);
    cout << "Get 3\n";
    o = ov.at(3);
    cout << "Get 4\n";
    o = ov.at(4);
    cout << "Get 5\n";
    o = ov.at(5);
}

jsonex::object create_2(jsonex::object o) {
    return o;
}

-(void) test_move_assignment
{
    jsonex::object o1, o2;
    
    o1 = std::move(o2);
}

- (void)testOperators
{
    jsonex::object o;
    
    XCTAssertTrue(o == nullptr, "null eq failed");
    XCTAssertFalse(o != nullptr, "null ne failed");
    
    o.set_value(1.0);
    XCTAssertTrue(o == 1.0, "double eq failed");
    XCTAssertTrue(o != 2.0, "double ne failed");
    XCTAssertTrue(o != nullptr, "double ne failed vs nullptr");
    XCTAssertTrue(o > 0.0, "double gt failed");
    XCTAssertTrue(o < 2.0, "double lt failed");
    XCTAssertTrue(o >= 0.0, "double ge failed");
    XCTAssertTrue(o <= 2.0, "double le failed");
    XCTAssertTrue(o == 1, "double eq int failed");
    XCTAssertTrue(o != 2, "double ne int failed");
    XCTAssertTrue(o > 0, "double gt int failed");
    XCTAssertTrue(o < 2, "double lt int failed");
    XCTAssertTrue(o >= 0, "double ge int failed");
    XCTAssertTrue(o <= 2, "double le int failed");
    
    o.set_value(1);
    XCTAssertTrue(o == 1, "int eq failed");
    XCTAssertTrue(o != 2, "int ne failed");
    XCTAssertTrue(o > 0, "int gt failed");
    XCTAssertTrue(o < 2, "int lt failed");
    XCTAssertTrue(o >= 0, "int ge failed");
    XCTAssertTrue(o <= 2.0, "int le double failed");
    XCTAssertTrue(o == 1.0, "int eq double failed");
    XCTAssertTrue(o != 2.0, "int ne double failed");
    XCTAssertTrue(o > 0.0, "int gt double failed");
    XCTAssertTrue(o < 2.0, "int lt double failed");
    XCTAssertTrue(o >= 0.0, "int ge double failed");
    XCTAssertTrue(o <= 2.0, "int le double failed");
    
    o.set_value("Hello, world!");
    XCTAssertTrue(o == "Hello, world!", "string eq failed");
    XCTAssertFalse(o != "Hello, world!", "string ne failed");
}

- (void) testChildren {
    
    // Create an object with an initializer list
    jsonex::object person {
        {"name", "Alice"},
        {"age", 24},
        {"height", 1.651},
        {"height-unit", "meter"}
    };
    
    XCTAssert(person.get_child2("name"), "Child property doesn't exist");
    XCTAssert(person.get_child2("name") == "Alice", "Child property value incorrect");
    // hack to catch C++ excceptions from objective-c
    try {
        person.get_child2("Doesn't exist");
        XCTAssert(0, "Failed to throw exception");
    }
    catch (const std::runtime_error &e) {
        
    }
    
    // Create another object to store some additional data
    jsonex::object physical;
    
    // Add some properties
    physical.add_child("weight", 54.4);
    
    physical.add_child("weight-unit", "kilogram");
    
    // Manipulate the original object, pull some properties out and put them
    // in the new object
    physical.transfer_child(person, "height");
    
//    XCTAssertFalse(person.has_child("height"));
//    XCTAssertTrue(physical.has_child("height"));
    
    physical.transfer_child(person, "height-unit");
    
    
    
    // Add the object into the person record
    //    person.add_child("physical", physical);
    //    XCTAssertTrue(physical.has_child("physical"));
}

-(void) test_to_string
{
    jsonex::object o{{"A", 1}, {"B", 2}};
    cout << o.to_string() << "\n";
    jsonex::object true_obj(true), false_obj(false), null_obj(nullptr);
    cout << true_obj.to_string() << "\n"
        << false_obj.to_string() << "\n"
        << null_obj.to_string() << "\n";

    jsonex::object o2{ {"A", jsonex::array{1, 2,3, 4}} };
    cout << o2.to_string() << "\n";

    jsonex::object o3 = jsonex::array{1, 2, 3, jsonex::array{1, 2, 3}, 4};
    cout << o3.to_string() << "\n";
}

-(void) test_get_value
{
    double d;
    int i;
    std::string s;
    bool result;
    
    jsonex::object o;
    
    o.set_value(1.5);
    XCTAssertTrue(o.is_number(), "Object kind set properly");
    result = o.get_value(d);
    XCTAssertTrue(result, "Get double value successfully");
    XCTAssertTrue(d == 1.5, "Get correct double value");
    
    result = o.get_value(i);
    XCTAssertTrue(result, "Get int value from double succesfully");
    XCTAssertTrue(i == 1, "Get correct (downcast) int value");
    
    result = o.get_value(s);
    XCTAssertFalse(result, "Do not get string value, object wrong type");

    
    o.set_value("Hello!");
    XCTAssertTrue(o.is_string(), "Object kind set properly");
    
    result = o.get_value(d);
    XCTAssertFalse(result, "No double value in object");
    
    result = o.get_value(i);
    XCTAssertFalse(result, "No int value in object");
    
    result = o.get_value(s);
    XCTAssertTrue(result, "Get string value successfully");
    XCTAssertTrue(s == "Hello!", "Get correct string value");
}

-(void) test_array_children
{
    jsonex::object o = jsonex::array{1, 2, 3, 4};
    XCTAssertTrue(o.children.size() == 4, "Incorrect number of children");
    XCTAssertTrue(o.get_child_index(0) == 1, "Child value incorrect");
    XCTAssertTrue(o.get_child_index(1) == 2, "Child value incorrect");
    XCTAssertTrue(o.get_child_index(2) == 3, "Child value incorrect");
    XCTAssertTrue(o.get_child_index(3) == 4, "Child value incorrect");
}

@end
