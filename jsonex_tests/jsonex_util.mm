//
//  jsonex_util.m
//  jsonex
//
//  Created by Seth Hill on 3/5/14.
//  Copyright (c) 2014 Seth Hill. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <iostream>
#include <string>
#include <vector>
#include "jsonex_utilities.h"

using std::string;
using std::vector;


@interface jsonex_util : XCTestCase

@end

@implementation jsonex_util

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_join
{
    vector<string> x{ "A", "BBB", "CC", "DDDD" };
    string output = jsonex::util::join(x, "--");
    XCTAssertTrue(output == "A--BBB--CC--DDDD", "Join failed");

    vector<string> y{ "A" };
    output = jsonex::util::join(y, "--");
    XCTAssertTrue(output == "A", "Join failed");

    vector<string> z;
    output = jsonex::util::join(z, "--");
    XCTAssertTrue(output == "", "Join failed");
}

-(void) test_encode
{
    string str = "ab\ncd";
    string test = "ab\\ncd";
    string processed;
    processed = jsonex::util::utf8_to_json(str);
    XCTAssertTrue(processed == test, "Encode simple escape");
    
    str = "abπcd";
    test = "ab\\u03c0cd";
    processed = jsonex::util::utf8_to_json(str, true);
    XCTAssertTrue(processed.compare(test) == 0, "Encode simple escape");

    str = u8"🎩";   // UTF8 = \xF0\x9F\x8E\xA9
    test = "\\ud83c\\udfa9"; // UTF16 = surrogate pairs D83C, DFA9
    processed = jsonex::util::utf8_to_json(str, true);
    XCTAssertTrue(processed.compare(test) == 0, "Encode simple escape");
}

@end
