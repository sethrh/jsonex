//
//  parser_test.m
//  jsonex
//
//  Created by Seth Hill on 3/7/14.
//  Copyright (c) 2014 Seth Hill. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "jsonex_parser.h"


@interface parser_test : XCTestCase

@end

@implementation parser_test

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void) test_find_number
{
    std::string str;
    std::string::const_iterator a, b, c;
    
    str = "   *(!~@*(*~*@*#AJOSIJDOAI";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == a, "not even close");
    
    str = R"(2.1, "c": false, "d": true, "e": null, "f": "string" }  )";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == a + 3, "Number with non-numbers following");
    
    str = "2067483925978642938475629378456923476529873465";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b);

    str = "0";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "Bare 0");
    
    str = "1e5";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "Valid exponential without decimal points or ±");
    
    str = "-0";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "-0");
    
    // invalid, >= 1 digit must follow .
    str = "0.";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == a, ">= 1 digit must follow .");
    
    str = "0.487501298346198237461928735419287345";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "leading 0 decimal");
    
    str = "1";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "valid number");
    
    str = "1.";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == a, ">= 1 digit must follow .");
    
    str = "-1";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "valid number");
    
    str = "-1.";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == a, ">= 1 digit must follow .");
    
    str = "-11897591083465981273645918273641";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b);

    str = "-1189759.1083465981273645918273641";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b);
    
    str = "-1189759.1083465981273645918273641e";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == a, "invalid exponential");
    
    str = "-1189759.1083465981273645918273641e+";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == a, "invalid exponential");
    
    str = "-1189759.1083465981273645918273641e-";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == a, "invalid exponential");
    
    str = "-1189759.1083465981273645918273641e+0";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "valid exponential");
    
    str = "-1189759.1083465981273645918273641e-0";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "valid exponential");
    
    str = "-1189759.1083465981273645918273641e+4921";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "valid exponential");

    str = "-1189759.1083465981273645918273641e-4921";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_number(a, b);
    XCTAssertTrue(c == b, "valid exponential");
}
#include <limits>
-(void) test_parse_number
{
    std::string str;
    std::string::const_iterator a, b;
    jsonex::object o;
    bool result;
    
    cout << std::numeric_limits<double>::max() << "\n";
    
    str = "   *(!~@*(*~*@*#AJOSIJDOAI";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertFalse(result, "expected invalid number");
    
    str = "1.18973e+4932";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertFalse(result, "Big number");
    
    str = "1.79769e+308";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertTrue(o.object_kind == jsonex::kind_number, "Is number");
    XCTAssertTrue(result, "Less big number");
    
    str = "1.0";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertTrue(result, "Small number");
    XCTAssertTrue(o.object_kind == jsonex::kind_number, "Is number");
    // The casting trick mentioned below doesn't work with C++ objects and
    // overloaded operators!
    XCTAssertTrue(o == 1.0, "Small number");
    
    str = "1";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertTrue(result, "Small number");
    XCTAssertTrue(o.object_kind == jsonex::kind_number, "Is number");
    XCTAssertTrue(o == 1.0, "Small number");
}

-(void) test_parse_string
{
    jsonex::object o;
    std::string str;
    std::string::const_iterator a, b, c;
    bool result;
    
    str = "\"quoted string\"";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_string(a, b, o);
    XCTAssertTrue(result, "Return true on valid string");
    XCTAssertTrue(o.object_kind == jsonex::kind_string, "Object kind is string");
    // reset the iterators, since parse string will change them
    a = str.cbegin() + 1;
    b = str.cend() - 1;
    XCTAssertTrue(o == std::string(a, b), "Object value doesn't contain quotes");
}

-(void) test_unicode_escapes {
    std::string str;
    std::string test;
    std::string processed;
    
    str = "\\u002f";
    test = "/";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Unicode escape");
    
    str = "\\u03c0";
    test = u8"π";   // \xCF80, pi
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Unicode escape");
    
    str = "\\u2318";
    test = u8"⌘";  // \xE2\x8C\x98, place of interest sign
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Unicode escape");
    
    str = "\\uD83C\\uDFA9";
    test = u8"🎩";  // \xF0\x9F\x8E\xA9, Top hat
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Unicode escape");
    
    // unicode at beginning
    str = "\\uD83C\\uDFA9blah blah blah";
    test = u8"🎩blah blah blah";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Unicode escape");
    
    // unicode at end
    str = "blah blah blah\\uD83C\\uDFA9";
    test = u8"blah blah blah🎩";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Unicode escape");
}

- (void) test_simple_escapes
{
    std::string str;
    std::string test;
    std::string processed;
    
    str = "ab";
    test = "ab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "No escapes");
    
    str = "bargle\\b\\\\\\n\\n";
    test = "bargle\b\\\n\n";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Lots of escapes");
    
    str = "ab\\\\";
    test = "ab\\";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Tailing backslash");
    
    str = "ab\\/";
    test = "ab/";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Trailing forward slash");
    
    str = "ab\\b";
    test = "ab\b";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Trailing backspace");
    
    str = "ab\\f";
    test = "ab\f";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Trailing formfeed");
    
    str = "ab\\n";
    test = "ab\n";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Trailing newline");
    
    str = "ab\\r";
    test = "ab\r";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Trailing carriage return (yay typewriters!");
    
    str = "ab\\t";
    test = "ab\t";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Leading tab");
    
    str = "\\\\ab";
    test = "\\ab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Leading backslash");
    
    str = "\\/ab";
    test = "/ab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Leading forward slash");
    
    str = "\\bab";
    test = "\bab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Leading backspace");
    
    str = "\\fab";
    test = "\fab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Leading formfeed");
    
    str = "\\nab";
    test = "\nab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Leading newline");
    
    str = "\\rab";
    test = "\rab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Leading carriage return");
    
    str = "\\tab";
    test = "\tab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Leading tab");

    str = "ab\\t";
    test = "ab\t";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Leading tab");
    
    str = "ab\\\\ab";
    test = "ab\\ab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Internal backslash");
    
    str = "ab\\/ab";
    test = "ab/ab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Internal forward slash");
    
    str = "ab\\bab";
    test = "ab\bab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Internal backspace");
    
    str = "ab\\fab";
    test = "ab\fab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Internal formfeed");
    
    str = "ab\\nab";
    test = "ab\nab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Internal newline");
    
    str = "ab\\rab";
    test = "ab\rab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Internal carriage return");
    
    str = "ab\\tab";
    test = "ab\tab";
    processed = jsonex::util::json_to_utf8(str);
    XCTAssertTrue(processed == test, "Internal tab");
}

-(void) test_find_word {
    std::string str, tmp;
    std::string::const_iterator a, b, c;

    str = "ioajsdoiajsdoiasjdah";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    XCTAssertTrue(c == b, "Valid word");

    // Should we return a valid word up to a bad character, or fail to find
    // anything?
    str = "abc\001def";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    tmp = std::string(a, c);
    XCTAssertEqual(tmp.length(), 3ul, "invalid string length");
    XCTAssertTrue(tmp == "abc", "invalid string");
    XCTAssertTrue(c == a + 3, "Invalid word, with a control char");

    str = "abc\"def";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    tmp = std::string(a, c);
    /* Note: Bare numbers in XCT have to have the same type and value to
     compare equal! The first value here is (silently) coerced to a NSUInteger,
     aka unsigned long. A bare '3' is a signed int, so they are deemed unequal. 
     Casting 3 to a NSUInteger -> "(NSUInteger)3" or adding the ul suffix 
     promotes 3 to an unsigned long, and XCT will finally be convinced of 
     equality.
     
     See: http://stackoverflow.com/a/19415285/65295
     */
    XCTAssertEqual(tmp.length(), 3ul, "invalid string length");
    XCTAssertTrue(tmp == "abc", "invalid string");
    XCTAssertTrue(c == a + 3, "Invalid word, with a double quote");

    str = "abc\\adef";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    tmp = std::string(a, c);
    XCTAssertEqual(tmp.length(), 3ul, "invalid string length");
    XCTAssertTrue(tmp == "abc", "invalid string");
    XCTAssertTrue(c == a + 3, "Word with invalid escape (\\a)");

    str = "\"";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    XCTAssertTrue(c == a, "String consisting of only a double quote");

    str = "\a";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    XCTAssertTrue(c == a, "String consisting of only a control char");

    str = "\\a";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    XCTAssertTrue(c == a, "String consisting of only an invalid escape");

    str = "\\";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    XCTAssertTrue(c == a, "String consisting of only an escape char");
    
    str = "\\n";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    XCTAssertTrue(c == b, "String consisting of only a valid escape");
    
    str = "\\u1234";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    XCTAssertTrue(c == b, "String consisting of a unicode escape");
    
    str = "\\uDEFG";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    XCTAssertTrue(c == a, "String consisting of invalid unicode escape");

    str = "";
    a = str.cbegin();
    b = str.cend();
    c = jsonex::recursive_parser::__find_word(a, b);
    XCTAssertTrue(c == a, "Empty");
}

-(void) test_parse_bool_or_null
{
    std::string str;
    std::string::const_iterator a, b;
    jsonex::object o;
    
    str = "true";
    a = str.cbegin();
    b = str.cend();
    XCTAssertTrue(jsonex::recursive_parser::__parse_null_or_bool(a, b, o), "True failed to parse");
    XCTAssertTrue(a == b, "Iterator didn't advance to end");
    XCTAssertTrue(o.is_true(), "True object isn't");

    str = "false";
    a = str.cbegin();
    b = str.cend();
    XCTAssertTrue(jsonex::recursive_parser::__parse_null_or_bool(a, b, o), "False failed to parse");
    XCTAssertTrue(a == b, "Iterator didn't advance to end");
    XCTAssertTrue(o.is_false(), "False object isn't");

    str = "null";
    a = str.cbegin();
    b = str.cend();
    XCTAssertTrue(jsonex::recursive_parser::__parse_null_or_bool(a, b, o), "Null failed to parse");
    XCTAssertTrue(a == b, "Iterator didn't advance to end");
    XCTAssertTrue(o.is_null(), "Null object isn't");
    
    // too short
    str = "tru";
    a = str.cbegin();
    b = str.cend();
    XCTAssertFalse(jsonex::recursive_parser::__parse_null_or_bool(a, b, o), "Invalid test didn't fail");

    // too long
    str = "truetrue";
    a = str.cbegin();
    b = str.cend();
    XCTAssertFalse(jsonex::recursive_parser::__parse_null_or_bool(a, b, o), "Invalid test didn't fail");
    
    // empty
    str = "";
    a = str.cbegin();
    b = str.cend();
    XCTAssertFalse(jsonex::recursive_parser::__parse_null_or_bool(a, b, o), "Invalid test didn't fail");

    // invalid
    str = "\\";
    a = str.cbegin();
    b = str.cend();
    XCTAssertFalse(jsonex::recursive_parser::__parse_null_or_bool(a, b, o), "Invalid test didn't fail");

    // garbage
    str = "9024jtqo;i4gjs;ofijg";
    a = str.cbegin();
    b = str.cend();
    XCTAssertFalse(jsonex::recursive_parser::__parse_null_or_bool(a, b, o), "Invalid test didn't fail");
    
    // unicode
    str = "日本の";
    a = str.cbegin();
    b = str.cend();
    XCTAssertFalse(jsonex::recursive_parser::__parse_null_or_bool(a, b, o), "Invalid test didn't fail");
    

}

-(void) test_bare_values
{
    // A double
    jsonex::object o = jsonex::recursive_parser::from_string(" 4.5  ");
    
    XCTAssertTrue(o.is_number(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == 4.5, "Incorrect object value");

    // an int
    o = jsonex::recursive_parser::from_string(" 4 ");
    
    XCTAssertTrue(o.is_number(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == 4, "Incorrect object value");
    
    // false
    o = jsonex::recursive_parser::from_string(" false");
    
    XCTAssertTrue(o.is_false(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    
    // true
    o = jsonex::recursive_parser::from_string("true");
    
    XCTAssertTrue(o.is_true(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    
    // null
    o = jsonex::recursive_parser::from_string(" null ");
    
    XCTAssertTrue(o.is_null(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    
    // string
    o = jsonex::recursive_parser::from_string(" \"aisjd;oisjd\"");
    
    XCTAssertTrue(o.is_string(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == "aisjd;oisjd", "Incorrect object value");

    // empty string
    o = jsonex::recursive_parser::from_string(" \"\" ");
    
    XCTAssertTrue(o.is_string(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == "", "Incorrect object value");

    // escaped string
    o = jsonex::recursive_parser::from_string(" \"\\n\" ");
    
    XCTAssertTrue(o.is_string(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == "\n", "Incorrect object value");
}

-(void) test_garbage
{
    std::string s;
    std::string::const_iterator a;
    std::string::const_iterator b;
    jsonex::object o;
    bool result;
    
    s = "";
    a = s.cbegin();
    b = s.cend();
    result = jsonex::recursive_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "blank string");
    
    s = "]";
    a = s.cbegin();
    b = s.cend();
    result = jsonex::recursive_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "end of array");
    
    s = "}";
    a = s.cbegin(); b = s.cend();
    result = jsonex::recursive_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "end of object");
    
    s = ",";
    a = s.cbegin(); b = s.cend();
    result = jsonex::recursive_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "comma");
    
    s = ":";
    a = s.cbegin(); b = s.cend();
    result = jsonex::recursive_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "colon");
    
    s = "\001\002\003";
    a = s.cbegin(); b = s.cend();
    result = jsonex::recursive_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "random binary");
    
    s = "http://stackoverflow.com/questions/8292050/is-there-any-publically-accessible-json-data-source-to-test-with-real-world-data";
    a = s.cbegin(); b = s.cend();
    result = jsonex::recursive_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "a url");
}

-(void) test_empty_object
{
    jsonex::object o = jsonex::recursive_parser::from_string("{}");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_object, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
}

-(void) test_empty_object_with_whitespace
{
    jsonex::object o = jsonex::recursive_parser::from_string("  {  }  ");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_object, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
}

-(void) test_simple_objects
{
    jsonex::object o = jsonex::recursive_parser::from_string(R"(  { "a" : 1 , "b" : 2.1, "c": false, "d": true, "e": null, "f": "string" }  )");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_object, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 6, "Incorrect number of children");
    XCTAssertTrue(o.get_child2("a") == 1, "Incorrect child value");
    XCTAssertTrue(o.get_child2("b") == 2.1, "Incorrect child value");
    XCTAssertTrue(o.get_child2("c").is_false(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("d").is_true(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("e").is_null(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("f") == "string", "Incorrect child value");
}

-(void) test_nested_objects
{
    jsonex::object o = jsonex::recursive_parser::from_string(R"(  {
                                                          "a" : {
                                                                 "b" : 1,
                                                                 "c": 2,
                                                                 "d" : 3
                                                                 }
                                                        }  )");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_object, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 1ul, "Incorrect number of children");
    
    jsonex::object& o2 = o.get_child2("a");
    XCTAssertTrue(o2.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o2.get_child2("b") == 1, "Incorrect child value");
    XCTAssertTrue(o2.get_child2("c") == 2, "Incorrect child value");
    XCTAssertTrue(o2.get_child2("d") == 3, "Incorrect child value");
}

-(void) test_empty_array
{
    jsonex::object o = jsonex::recursive_parser::from_string("[]");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
}

-(void) test_array_of_empty_arrays
{
    jsonex::object o = jsonex::recursive_parser::from_string("[[],[],[]]");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o.get_child_index(0).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).object_kind == jsonex::kind_array, "Incorrect child");
}

-(void) test_int_array
{
    jsonex::object o = jsonex::recursive_parser::from_string("[0, 1, 2]");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o.get_child_index(0) == 0, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1) == 1, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2) == 2, "Incorrect child");
}

-(void) test_int_array_with_spaces
{
    jsonex::object o = jsonex::recursive_parser::from_string(" [ 0 , 1 , 2 ] ");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o.get_child_index(0) == 0, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1) == 1, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2) == 2, "Incorrect child");
}

-(void) test_array_of_int_arrays
{
    jsonex::object o = jsonex::recursive_parser::from_string("[[1, 2, 3], [4, 5, 6], [7, 8, 9]]");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o.get_child_index(0).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(0).children.size() == 3, "Incorrect child");
    XCTAssertTrue(o.get_child_index(0).get_child_index(0) == 1, "Incorrect child");
    XCTAssertTrue(o.get_child_index(0).get_child_index(1) == 2, "Incorrect child");
    XCTAssertTrue(o.get_child_index(0).get_child_index(2) == 3, "Incorrect child");
    
    XCTAssertTrue(o.get_child_index(1).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).children.size() == 3, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).get_child_index(0) == 4, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).get_child_index(1) == 5, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).get_child_index(2) == 6, "Incorrect child");
    
    XCTAssertTrue(o.get_child_index(2).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).children.size() == 3, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).get_child_index(0) == 7, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).get_child_index(1) == 8, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).get_child_index(2) == 9, "Incorrect child");
}


-(void) test_complex
{
    std::string s = R"({
        "key_0": 2.1249,
        "key_1a": 1e5,
        "key_2": 148,
        "key_3": "bargles",
        "key_4": false,
        "key_5": true,
        "key_6": null,
        "key_7": [1, 2, 3],
        "key_8": {"A": 1, "B": [1, 2, 3], "C": {"a": 1, "b": 2}}
    })";

    jsonex::object o = jsonex::recursive_parser::from_string(s);

    XCTAssertTrue(o.is_object(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 9, "Incorrect number of children");
    XCTAssertTrue(o.get_child2("key_0") == 2.1249, "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_1a") == 1e5, "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_2") == 148, "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_3") == "bargles", "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_4").is_false(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_5").is_true(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_6").is_null(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_7").is_array(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_7").children.size() == 3, "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_8").is_object(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_8").children.size() == 3, "Incorrect child value");

}

-(void) test_complex_with_utf8
{
    // A test string, containing a UTF-8 character.
    std::string s = R"({
        "key_字": 2.1249,
        "key_1a": 1e5,
        "key_2": 148,
        "key_3": "barglesπ",
        "key_4": false,
        "key_5": true,
        "key_6": null,
        "key_7": [1, 2, 3],
        "key_8": {"A": 1, "B": [1, 2, 3], "C": {"a": 1, "b": 2}}
    })";

    jsonex::object o = jsonex::recursive_parser::from_string(s);
    XCTAssertTrue(o.is_object(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 9, "Incorrect number of children");
    XCTAssertTrue(o.get_child2("key_字") == 2.1249, "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_1a") == 1e5, "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_2") == 148, "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_3") == u8"barglesπ", "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_4").is_false(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_5").is_true(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_6").is_null(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_7").is_array(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_7").children.size() == 3, "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_8").is_object(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("key_8").children.size() == 3, "Incorrect child value");

}

-(void) test_huge
{
    // A 3KB test string stolen from cJSON
    std::string s = R"({
    "web-app": {
        "servlet": [
                    {
                        "init-param": {
                            "cachePackageTagsRefresh": 60,
                            "cachePackageTagsStore": 200,
                            "cachePackageTagsTrack": 200,
                            "cachePagesDirtyRead": 10,
                            "cachePagesRefresh": 10,
                            "cachePagesStore": 100,
                            "cachePagesTrack": 200,
                            "cacheTemplatesRefresh": 15,
                            "cacheTemplatesStore": 50,
                            "cacheTemplatesTrack": 100,
                            "configGlossary:adminEmail": "ksm@pobox.com",
                            "configGlossary:installationAt": "Philadelphia, PA",
                            "configGlossary:poweredBy": "Cofax",
                            "configGlossary:poweredByIcon": "/images/cofax.gif",
                            "configGlossary:staticPath": "/content/static",
                            "dataStoreClass": "org.cofax.SqlDataStore",
                            "dataStoreConnUsageLimit": 100,
                            "dataStoreDriver": "com.microsoft.jdbc.sqlserver.SQLServerDriver",
                            "dataStoreInitConns": 10,
                            "dataStoreLogFile": "/usr/local/tomcat/logs/datastore.log",
                            "dataStoreLogLevel": "debug",
                            "dataStoreMaxConns": 100,
                            "dataStoreName": "cofax",
                            "dataStorePassword": "dataStoreTestQuery",
                            "dataStoreTestQuery": "SET NOCOUNT ON;select test='test';",
                            "dataStoreUrl": "jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon",
                            "dataStoreUser": "sa",
                            "defaultFileTemplate": "articleTemplate.htm",
                            "defaultListTemplate": "listTemplate.htm",
                            "jspFileTemplate": "articleTemplate.jsp",
                            "jspListTemplate": "listTemplate.jsp",
                            "maxUrlLength": 500,
                            "redirectionClass": "org.cofax.SqlRedirection",
                            "searchEngineFileTemplate": "forSearchEngines.htm",
                            "searchEngineListTemplate": "forSearchEnginesList.htm",
                            "searchEngineRobotsDb": "WEB-INF/robots.db",
                            "templateLoaderClass": "org.cofax.FilesTemplateLoader",
                            "templateOverridePath": "",
                            "templatePath": "templates",
                            "templateProcessorClass": "org.cofax.WysiwygTemplate",
                            "useDataStore": true,
                            "useJSP": false
                        },
                        "servlet-class": "org.cofax.cds.CDSServlet",
                        "servlet-name": "cofaxCDS"
                    },
                    {
                        "init-param": {
                            "mailHost": "mail1",
                            "mailHostOverride": "mail2"
                        },
                        "servlet-class": "org.cofax.cds.EmailServlet",
                        "servlet-name": "cofaxEmail"
                    },
                    {
                        "servlet-class": "org.cofax.cds.AdminServlet",
                        "servlet-name": "cofaxAdmin"
                    },
                    {
                        "servlet-class": "org.cofax.cds.FileServlet",
                        "servlet-name": "fileServlet"
                    },
                    {
                        "init-param": {
                            "adminGroupID": 4,
                            "betaServer": true,
                            "dataLog": 1,
                            "dataLogLocation": "/usr/local/tomcat/logs/dataLog.log",
                            "dataLogMaxSize": "",
                            "fileTransferFolder": "/usr/local/tomcat/webapps/content/fileTransferFolder",
                            "log": 1,
                            "logLocation": "/usr/local/tomcat/logs/CofaxTools.log",
                            "logMaxSize": "",
                            "lookInContext": 1,
                            "removePageCache": "/content/admin/remove?cache=pages&id=",
                            "removeTemplateCache": "/content/admin/remove?cache=templates&id=",
                            "templatePath": "toolstemplates/"
                        },
                        "servlet-class": "org.cofax.cms.CofaxToolsServlet",
                        "servlet-name": "cofaxTools"
                    }
                    ],
        "servlet-mapping": {
            "cofaxAdmin": "/admin/*",
            "cofaxCDS": "/",
            "cofaxEmail": "/cofaxutil/aemail/*",
            "cofaxTools": "/tools/*",
            "fileServlet": "/static/*"
        },
        "taglib": {
            "taglib-location": "/WEB-INF/tlds/cofax.tld",
            "taglib-uri": "cofax.tld"
        }
    }
}
)";

    jsonex::object o = jsonex::recursive_parser::from_string(s);
    XCTAssertTrue(o.is_object(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 1, "Incorrect number of children");
    XCTAssertTrue(o.get_child2("web-app").is_object(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("web-app").children.size() == 3, "Incorrect child value");
    XCTAssertTrue(o.get_child2("web-app").get_child2("servlet").is_array(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("web-app").get_child2("servlet").children.size() == 5, "Incorrect child value");
    // etc
}

-(void) test_dangling_string
{
    std::string s = "[4,\"]";
    jsonex::object o = jsonex::recursive_parser::from_string(s);
    XCTAssertTrue(!o.is_valid(), "not valid");
    
    s = "[\"hi";
    o = jsonex::recursive_parser::from_string(s);
    XCTAssertFalse(o.is_valid(), "not valid");
    
    s = "{\"hi";
    o = jsonex::recursive_parser::from_string(s);
    XCTAssertFalse(o.is_valid(), "not valid");

    s = "{\"hi\": \"";
    o = jsonex::recursive_parser::from_string(s);
    XCTAssertFalse(o.is_valid(), "not valid");

    s = "\"hi";
    o = jsonex::recursive_parser::from_string(s);
    XCTAssertFalse(o.is_valid(), "not valid");
}


//#define TEST_ATTACK_VECTORS
#ifdef TEST_ATTACK_VECTORS
-(void) test_attack_vectors
{
    /* The parser is recursive, so you can currently crash it by giving it bogus
     input data, if you know that it's recursive. 
     
     Safari / WebKit, for example, can handle at least 200 KB of '[' just fine.
     
     This could be corrected by a "is_this_really_an_array()" function which 
     would scan the string to make sure that the brackets are balanced. That 
     would be pretty fast and could be implemented non-recursively.
     */
    std::string s;
    std::string::const_iterator a, b;
    bool result;
    jsonex::object o;
    
    s = std::string(1e4, '[');
    XCTAssertTrue(s.length() == 1e4);
    a = s.cbegin(); b = s.cend();
    result = jsonex::recursive_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "a recursion bomb");
}
#endif 

@end
