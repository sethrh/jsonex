//
//  stack_parser_test.m
//  jsonex
//
//  Created by Seth Hill on 3/15/14.
//  Copyright (c) 2014 Seth Hill. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "jsonex_parser.h"

@interface stack_parser_test : XCTestCase

@end

@implementation stack_parser_test

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void) test_bare_values
{
    // A double
    jsonex::object o = jsonex::stack_parser::from_string(" 4.5  ");
    
    XCTAssertTrue(o.is_number(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == 4.5, "Incorrect object value");
    
    // an int
    o = jsonex::stack_parser::from_string(" 4 ");
    
    XCTAssertTrue(o.is_number(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == 4, "Incorrect object value");
    
    // false
    o = jsonex::stack_parser::from_string(" false");
    
    XCTAssertTrue(o.is_false(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    
    // true
    o = jsonex::stack_parser::from_string("true");
    
    XCTAssertTrue(o.is_true(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    
    // null
    o = jsonex::stack_parser::from_string(" null ");
    
    XCTAssertTrue(o.is_null(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    
    // string
    o = jsonex::stack_parser::from_string(" \"aisjd;oisjd\"");
    
    XCTAssertTrue(o.is_string(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == "aisjd;oisjd", "Incorrect object value");
    
    // empty string
    o = jsonex::stack_parser::from_string(" \"\" ");
    
    XCTAssertTrue(o.is_string(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == "", "Incorrect object value");
    
    // escaped string
    o = jsonex::stack_parser::from_string(" \"\\n\" ");
    
    XCTAssertTrue(o.is_string(), "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
    XCTAssertTrue(o == "\n", "Incorrect object value");
}

-(void) test_garbage
{
    std::string s;
    std::string::const_iterator a;
    std::string::const_iterator b;
    jsonex::object o;
    bool result;
    
    s = "";
    a = s.cbegin();
    b = s.cend();
    result = jsonex::stack_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "blank string");
    
    s = "]";
    a = s.cbegin();
    b = s.cend();
    result = jsonex::stack_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "end of array");
    
    s = "}";
    a = s.cbegin(); b = s.cend();
    result = jsonex::stack_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "end of object");
    
    s = ",";
    a = s.cbegin(); b = s.cend();
    result = jsonex::stack_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "comma");
    
    s = ":";
    a = s.cbegin(); b = s.cend();
    result = jsonex::stack_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "colon");
    
    s = "\001\002\003";
    a = s.cbegin(); b = s.cend();
    result = jsonex::stack_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "random binary");
    
    s = "http://stackoverflow.com/questions/8292050/is-there-any-publically-accessible-json-data-source-to-test-with-real-world-data";
    a = s.cbegin(); b = s.cend();
    result = jsonex::stack_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "a url");
}

-(void) test_empty_object
{
    jsonex::object o = jsonex::stack_parser::from_string("{}");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_object, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
}

-(void) test_empty_object_with_whitespace
{
    jsonex::object o = jsonex::stack_parser::from_string("  {  }  ");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_object, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
}

-(void) test_simple_objects
{
    jsonex::object o = jsonex::stack_parser::from_string(R"(  { "a" : 1 , "b" : 2.1, "c": false, "d": true, "e": null, "f": "string" }  )");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_object, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 6, "Incorrect number of children");
    XCTAssertTrue(o.get_child2("a") == 1, "Incorrect child value");
    XCTAssertTrue(o.get_child2("b") == 2.1, "Incorrect child value");
    XCTAssertTrue(o.get_child2("c").is_false(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("d").is_true(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("e").is_null(), "Incorrect child value");
    XCTAssertTrue(o.get_child2("f") == "string", "Incorrect child value");
}

-(void) test_nested_objects
{
    jsonex::object o = jsonex::stack_parser::from_string(R"(  {
                                                             "a" : {
                                                                 "b" : 1,
                                                                 "c": 2,
                                                                 "d" : 3
                                                             }
                                                             }  )");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_object, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 1ul, "Incorrect number of children");
    
    jsonex::object& o2 = o.get_child2("a");
    XCTAssertTrue(o2.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o2.get_child2("b") == 1, "Incorrect child value");
    XCTAssertTrue(o2.get_child2("c") == 2, "Incorrect child value");
    XCTAssertTrue(o2.get_child2("d") == 3, "Incorrect child value");
}

-(void) test_empty_array
{
    jsonex::object o = jsonex::stack_parser::from_string("[]");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 0, "Incorrect number of children");
}

-(void) test_array_of_empty_arrays
{
    jsonex::object o = jsonex::stack_parser::from_string("[[],[],[]]");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o.get_child_index(0).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).object_kind == jsonex::kind_array, "Incorrect child");
}

-(void) test_int_array
{
    jsonex::object o = jsonex::stack_parser::from_string("[0, 1, 2]");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o.get_child_index(0) == 0, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1) == 1, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2) == 2, "Incorrect child");
}

-(void) test_int_array_with_spaces
{
    jsonex::object o = jsonex::stack_parser::from_string(" [ 0 , 1 , 2 ] ");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o.get_child_index(0) == 0, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1) == 1, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2) == 2, "Incorrect child");
}

-(void) test_array_of_int_arrays
{
    jsonex::object o = jsonex::stack_parser::from_string("[[1, 2, 3], [4, 5, 6], [7, 8, 9]]");
    
    XCTAssertTrue(o.object_kind == jsonex::kind_array, "Incorrect kind");
    XCTAssertTrue(o.children.size() == 3, "Incorrect number of children");
    XCTAssertTrue(o.get_child_index(0).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(0).children.size() == 3, "Incorrect child");
    XCTAssertTrue(o.get_child_index(0).get_child_index(0) == 1, "Incorrect child");
    XCTAssertTrue(o.get_child_index(0).get_child_index(1) == 2, "Incorrect child");
    XCTAssertTrue(o.get_child_index(0).get_child_index(2) == 3, "Incorrect child");
    
    XCTAssertTrue(o.get_child_index(1).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).children.size() == 3, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).get_child_index(0) == 4, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).get_child_index(1) == 5, "Incorrect child");
    XCTAssertTrue(o.get_child_index(1).get_child_index(2) == 6, "Incorrect child");
    
    XCTAssertTrue(o.get_child_index(2).object_kind == jsonex::kind_array, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).children.size() == 3, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).get_child_index(0) == 7, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).get_child_index(1) == 8, "Incorrect child");
    XCTAssertTrue(o.get_child_index(2).get_child_index(2) == 9, "Incorrect child");
}


-(void) test_complex
{
    std::string s = R"({
    "key_0": 2.1249,
    "key_1a": 1e5,
    "key_2": 148,
    "key_3": "bargles",
    "key_4": false,
    "key_5": true,
    "key_6": null,
    "key_7": [1, 2, 3],
    "key_8": {"A": 1, "B": [1, 2, 3], "C": {"a": 1, "b": 2}}
})";

jsonex::object o = jsonex::stack_parser::from_string(s);

XCTAssertTrue(o.is_object(), "Incorrect kind");
XCTAssertTrue(o.children.size() == 9, "Incorrect number of children");
XCTAssertTrue(o.get_child2("key_0") == 2.1249, "Incorrect child value");
XCTAssertTrue(o.get_child2("key_1a") == 1e5, "Incorrect child value");
XCTAssertTrue(o.get_child2("key_2") == 148, "Incorrect child value");
XCTAssertTrue(o.get_child2("key_3") == "bargles", "Incorrect child value");
XCTAssertTrue(o.get_child2("key_4").is_false(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_5").is_true(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_6").is_null(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_7").is_array(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_7").children.size() == 3, "Incorrect child value");
XCTAssertTrue(o.get_child2("key_8").is_object(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_8").children.size() == 3, "Incorrect child value");

}

-(void) test_complex_with_utf8
{
    // A test string, containing a UTF-8 character.
    std::string s = R"({
    "key_字": 2.1249,
    "key_1a": 1e5,
    "key_2": 148,
    "key_3": "barglesπ",
    "key_4": false,
    "key_5": true,
    "key_6": null,
    "key_7": [1, 2, 3],
    "key_8": {"A": 1, "B": [1, 2, 3], "C": {"a": 1, "b": 2}}
})";

jsonex::object o = jsonex::stack_parser::from_string(s);
XCTAssertTrue(o.is_object(), "Incorrect kind");
XCTAssertTrue(o.children.size() == 9, "Incorrect number of children");
XCTAssertTrue(o.get_child2("key_字") == 2.1249, "Incorrect child value");
XCTAssertTrue(o.get_child2("key_1a") == 1e5, "Incorrect child value");
XCTAssertTrue(o.get_child2("key_2") == 148, "Incorrect child value");
XCTAssertTrue(o.get_child2("key_3") == u8"barglesπ", "Incorrect child value");
XCTAssertTrue(o.get_child2("key_4").is_false(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_5").is_true(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_6").is_null(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_7").is_array(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_7").children.size() == 3, "Incorrect child value");
XCTAssertTrue(o.get_child2("key_8").is_object(), "Incorrect child value");
XCTAssertTrue(o.get_child2("key_8").children.size() == 3, "Incorrect child value");

}

-(void) test_huge
{
    // A 3KB test string stolen from cJSON
    std::string s = R"({
    "web-app": {
        "servlet": [
                    {
                        "init-param": {
                            "cachePackageTagsRefresh": 60,
                            "cachePackageTagsStore": 200,
                            "cachePackageTagsTrack": 200,
                            "cachePagesDirtyRead": 10,
                            "cachePagesRefresh": 10,
                            "cachePagesStore": 100,
                            "cachePagesTrack": 200,
                            "cacheTemplatesRefresh": 15,
                            "cacheTemplatesStore": 50,
                            "cacheTemplatesTrack": 100,
                            "configGlossary:adminEmail": "ksm@pobox.com",
                            "configGlossary:installationAt": "Philadelphia, PA",
                            "configGlossary:poweredBy": "Cofax",
                            "configGlossary:poweredByIcon": "/images/cofax.gif",
                            "configGlossary:staticPath": "/content/static",
                            "dataStoreClass": "org.cofax.SqlDataStore",
                            "dataStoreConnUsageLimit": 100,
                            "dataStoreDriver": "com.microsoft.jdbc.sqlserver.SQLServerDriver",
                            "dataStoreInitConns": 10,
                            "dataStoreLogFile": "/usr/local/tomcat/logs/datastore.log",
                            "dataStoreLogLevel": "debug",
                            "dataStoreMaxConns": 100,
                            "dataStoreName": "cofax",
                            "dataStorePassword": "dataStoreTestQuery",
                            "dataStoreTestQuery": "SET NOCOUNT ON;select test='test';",
                            "dataStoreUrl": "jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon",
                            "dataStoreUser": "sa",
                            "defaultFileTemplate": "articleTemplate.htm",
                            "defaultListTemplate": "listTemplate.htm",
                            "jspFileTemplate": "articleTemplate.jsp",
                            "jspListTemplate": "listTemplate.jsp",
                            "maxUrlLength": 500,
                            "redirectionClass": "org.cofax.SqlRedirection",
                            "searchEngineFileTemplate": "forSearchEngines.htm",
                            "searchEngineListTemplate": "forSearchEnginesList.htm",
                            "searchEngineRobotsDb": "WEB-INF/robots.db",
                            "templateLoaderClass": "org.cofax.FilesTemplateLoader",
                            "templateOverridePath": "",
                            "templatePath": "templates",
                            "templateProcessorClass": "org.cofax.WysiwygTemplate",
                            "useDataStore": true,
                            "useJSP": false
                        },
                        "servlet-class": "org.cofax.cds.CDSServlet",
                        "servlet-name": "cofaxCDS"
                    },
                    {
                        "init-param": {
                            "mailHost": "mail1",
                            "mailHostOverride": "mail2"
                        },
                        "servlet-class": "org.cofax.cds.EmailServlet",
                        "servlet-name": "cofaxEmail"
                    },
                    {
                        "servlet-class": "org.cofax.cds.AdminServlet",
                        "servlet-name": "cofaxAdmin"
                    },
                    {
                        "servlet-class": "org.cofax.cds.FileServlet",
                        "servlet-name": "fileServlet"
                    },
                    {
                        "init-param": {
                            "adminGroupID": 4,
                            "betaServer": true,
                            "dataLog": 1,
                            "dataLogLocation": "/usr/local/tomcat/logs/dataLog.log",
                            "dataLogMaxSize": "",
                            "fileTransferFolder": "/usr/local/tomcat/webapps/content/fileTransferFolder",
                            "log": 1,
                            "logLocation": "/usr/local/tomcat/logs/CofaxTools.log",
                            "logMaxSize": "",
                            "lookInContext": 1,
                            "removePageCache": "/content/admin/remove?cache=pages&id=",
                            "removeTemplateCache": "/content/admin/remove?cache=templates&id=",
                            "templatePath": "toolstemplates/"
                        },
                        "servlet-class": "org.cofax.cms.CofaxToolsServlet",
                        "servlet-name": "cofaxTools"
                    }
                    ],
        "servlet-mapping": {
            "cofaxAdmin": "/admin/*",
            "cofaxCDS": "/",
            "cofaxEmail": "/cofaxutil/aemail/*",
            "cofaxTools": "/tools/*",
            "fileServlet": "/static/*"
        },
        "taglib": {
            "taglib-location": "/WEB-INF/tlds/cofax.tld",
            "taglib-uri": "cofax.tld"
        }
    }
}
)";

jsonex::object o = jsonex::stack_parser::from_string(s);
XCTAssertTrue(o.is_object(), "Incorrect kind");
XCTAssertTrue(o.children.size() == 1, "Incorrect number of children");
XCTAssertTrue(o.get_child2("web-app").is_object(), "Incorrect child value");
XCTAssertTrue(o.get_child2("web-app").children.size() == 3, "Incorrect child value");
XCTAssertTrue(o.get_child2("web-app").get_child2("servlet").is_array(), "Incorrect child value");
XCTAssertTrue(o.get_child2("web-app").get_child2("servlet").children.size() == 5, "Incorrect child value");
// etc
}

-(void) test_dangling_string
{
    std::string s = "[1,\"]";
    jsonex::object o = jsonex::stack_parser::from_string(s);
    XCTAssertTrue(!o.is_valid(), "not valid");
    
    s = "[\"test]";
    o = jsonex::stack_parser::from_string(s);
    XCTAssertTrue(!o.is_valid(), "not valid");
    
    s = "[\"hi";
    o = jsonex::recursive_parser::from_string(s);
    XCTAssertFalse(o.is_valid(), "not valid");
    
    s = "{\"hi";
    o = jsonex::recursive_parser::from_string(s);
    XCTAssertFalse(o.is_valid(), "not valid");
    
    s = "{\"hi\": \"";
    o = jsonex::recursive_parser::from_string(s);
    XCTAssertFalse(o.is_valid(), "not valid");
    
    s = "\"hi";
    o = jsonex::recursive_parser::from_string(s);
    XCTAssertFalse(o.is_valid(), "not valid");
    
}

#include <limits>
-(void) test_numbers
{
    std::string str;
    std::string::const_iterator a, b;
    jsonex::object o;
    bool result;

    str = "1";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::stack_parser::__parse_number(a, b, o);
    XCTAssertTrue(result);
    XCTAssertTrue(o.is_number(), "1");
    
    str = "-0";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::stack_parser::__parse_number(a, b, o);
    XCTAssertTrue(result);
    
    str = "0";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::stack_parser::__parse_number(a, b, o);
    XCTAssertTrue(result);

    str = "01";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::stack_parser::__parse_number(a, b, o);
    XCTAssertFalse(result);

    str = "-01";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::stack_parser::__parse_number(a, b, o);
    XCTAssertFalse(result);
    
    // This should yield "1" and leave "-" in the bucket
    str = "1-";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::stack_parser::__parse_number(a, b, o);
    XCTAssertTrue(result);
    XCTAssertTrue(*a == '-');
    
    str = "+";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::stack_parser::__parse_number(a, b, o);
    XCTAssertFalse(result);

    str = "+1";
    o = jsonex::recursive_parser::from_string(str);
    XCTAssertFalse(o.is_number(), "+1");

    
    
    
    str = "   *(!~@*(*~*@*#AJOSIJDOAI";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertFalse(result, "expected invalid number");
    
    str = "1.18973e+4932";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertFalse(result, "Big number");
    
    str = "1.79769e+308";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertTrue(o.object_kind == jsonex::kind_number, "Is number");
    XCTAssertTrue(result, "Less big number");
    
    str = "1.0";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertTrue(result, "Small number");
    XCTAssertTrue(o.object_kind == jsonex::kind_number, "Is number");
    // The casting trick mentioned below doesn't work with C++ objects and
    // overloaded operators!
    XCTAssertTrue(o == 1.0, "Small number");
    
    str = "1";
    a = str.cbegin();
    b = str.cend();
    result = jsonex::recursive_parser::__parse_number(a, b, o);
    XCTAssertTrue(result, "Small number");
    XCTAssertTrue(o.object_kind == jsonex::kind_number, "Is number");
    XCTAssertTrue(o == 1.0, "Small number");
}
    



-(void) test_attack_vectors
{
    /* The parser is recursive, so you can currently crash it by giving it bogus
     input data, if you know that it's recursive.
     
     Safari / WebKit, for example, can handle at least 200 KB of '[' just fine.
     
     This could be corrected by a "is_this_really_an_array()" function which
     would scan the string to make sure that the brackets are balanced. That
     would be pretty fast and could be implemented non-recursively.
     */
    std::string s;
    std::string::const_iterator a, b;
    bool result;
    jsonex::object o;
    
    s = std::string(1e4, '[');
    XCTAssertTrue(s.length() == 1e4);
    a = s.cbegin(); b = s.cend();
    result = jsonex::stack_parser::__parse(a, b, o);
    XCTAssertTrue(result == false, "a recursion bomb");
}

@end
