//
//  jsonex_utilities.cpp
//  jsonex
//
//  Created by Seth Hill on 3/6/14.
//  Copyright (c) 2014 Seth Hill. All rights reserved.
//

#include <codecvt>
#include <locale>
#include <numeric>
#include <string>
#include <vector>

#include "jsonex_utilities.h"

using std::accumulate;
using std::vector;
using std::string;

namespace jsonex { namespace util {
    
    /** Generic vector<string> join function.
     join(vec{"A", "B", "C"}, "-") => "A-B-C" */
    
    string::size_type size_sum(const string::size_type &a, const string &b) {
        return a + b.length();
    }
    
    string join(vector<string> vec, string sep) {
        //        string::size_type expected_size;
        //        expected_size = accumulate(vec.begin(), vec.end(), string::size_type(0), size_sum)
        //                        + sep.length() * (vec.size() - 1);
        //
        //        string output;
        //        output.reserve(expected_size);
        string output;
        
        if (vec.size() == 0) {
            return "";
        }
        
        if (vec.size() == 1) {
            return vec.back();
        }
        
        for (auto it = vec.begin(); it != vec.end() - 1; it++) {
            output += *it;
            output += sep;
        }
        
        output += vec.back();
        
        return output;
    }
    
    /** Scan through the string, locate and process JSON escape sequences.
     Returns the original, unmodified string if an invalid escape is
     encountered.
     
     It might be a better idea to change the signature to
     bool (const string &in, string &out)
     and return false on invalid escapes.
     
     For primary usage, invalid escapes will cause failures in __find_word,
     so we needn't worry _too_ much about them here...
     */
    string json_to_utf8(const string &str) {
        std::string processed;
        std::string escape;
        std::string::const_iterator start = str.cbegin();
        std::string::const_iterator iter = str.cbegin();
        std::string::const_iterator end = str.cend();
        
        processed.reserve(str.length());
        
        while (iter < end) {
            // non-escape, just advance
            if (*iter != '\\') {
                iter++;
                continue;
            }
            
            // Find a backslash & translate escape
            else {
                
                // silly, backslash at the end of input
                if ((iter + 1) == end) {
                    return str;
                }
                
                // unicode escape
                if (*(iter + 1) == 'u') {
                    if (iter + 5 >= end ||
                        !std::isxdigit(*(iter + 2)) ||
                        !std::isxdigit(*(iter + 3)) ||
                        !std::isxdigit(*(iter + 4)) ||
                        !std::isxdigit(*(iter + 5)))
                    {
                        return str; // invalid unicode escape
                    }
                    
                    // UTF16 -> UTF8 conversion
                    int hex = std::stoi(string(iter + 2, iter + 6), nullptr, 16);
                    
                    // Above maximum code point?
                    // if (hex > 0x10ffff) {
                    // return str;
                    // }
                    
                    // Trailing surrogate?
                    if ((hex >= 0xDC00 && hex <= 0xDFFF) || hex == 0) {
                        return str;
                    }
                    
                    if (hex >= 0xD800 && hex <= 0xDBFF) // UTF 16 surrogate pairs
                    {
                        const std::string::const_iterator pair_iter = iter + 6;
                        // Get the other half of the pair
                        if (pair_iter + 5 >= end ||
                            *(pair_iter) != '\\' ||
                            *(pair_iter + 1) != 'u' ||
                            !std::isxdigit(*(pair_iter + 2)) ||
                            !std::isxdigit(*(pair_iter + 3)) ||
                            !std::isxdigit(*(pair_iter + 4)) ||
                            !std::isxdigit(*(pair_iter + 5)))
                        {
                            return str; // Invalid surrogate pair
                        }
                        
                        int tail = std::stoi(string(pair_iter + 2, pair_iter + 6), nullptr, 16);
                        
                        if (tail < 0xdc00 || tail > 0xdfff) {
                            return str; // invalid tail
                        }
                        
                        hex = 0x10000 + (((hex & 0x3ff) << 10) | (tail & 0x3ff));
                    }
                    
                    // plain ascii / 1 byte UTF-8
                    if (hex < 0x80) {
                        escape = hex;
                    }
                    // 2 byte UTF-8
                    else if (hex < 0x800) {
                        escape = ((hex >> 6) & 0xFF) | 0xc0;
                        escape += (hex & 0x3f) | 0x80;
                    }
                    else if (hex < 0x10000) {
                        escape = ((hex >> 12) & 0xFF) | 0xe0;
                        escape += ((hex >> 6) & 0x3f) | 0x80;
                        escape += (hex & 0x3f) | 0x80;
                    }
                    else {
                        escape = (hex >> 18) | 0xf0;
                        escape += ((hex >> 12) & 0x3f) | 0x80;
                        escape += ((hex >> 6) & 0x3f) | 0x80;
                        escape += (hex & 0x3f) | 0x80;
                        
                    }
                    
                    // copy everything that we've found so far
                    processed.append(start, iter);
                    processed.append(escape);
                    
                    // mark our last position in the string and advance
                    if (hex >= 0x10000) {
                        iter += 12;
                    }
                    else {
                        iter += 6;
                    }
                    start = iter;
                }
                
                // non-unicode escpae
                else {
                    
                    switch (*(iter + 1)) {
                        case '\"':
                            escape = '\"';
                            break;
                        case '\\':
                            escape = '\\';
                            break;
                        case '/':
                            escape = '/';
                            break;
                        case 'b':
                            escape = '\b';
                            break;
                        case 'f':
                            escape = '\f';
                            break;
                        case 'n':
                            escape = '\n';
                            break;
                        case 'r':
                            escape = '\r';
                            break;
                        case 't':
                            escape = '\t';
                            break;
                            
                        default:
                            // invalid escape sequence
                            return str;
                    }
                    
                    // copy everything that we've found so far
                    processed.append(start, iter);
                    processed.append(escape);
                    
                    // mark our last position in the string and advance
                    iter += 2;
                    start = iter;
                }
            }
        }
        
        processed.append(start, iter);
        return processed;
    }


    /** Convert UTF-8 and newline/etc to escapes. */
    string utf8_to_json(const string &str, bool escape_unicode) {
        std::string processed;
        std::string escape;
        std::string::const_iterator start = str.cbegin();
        std::string::const_iterator iter = str.cbegin();
        std::string::const_iterator end = str.cend();
        
        processed.reserve(str.length() * 2);
        
        while (iter < end) {
            // Check if we should bother with escaping unicode.
            // It might also be nice to be able to escape certain code points,
            // such as everything > 0xFF, or everything within certain ranges.
            // It probably gets irritating real fast if your string full of
            // perfectly good 漢字 gets turned into a bunch of \u6f22\u5b57
            // For now, escape_unicode defaults to false, so nothing will be
            // escaped in json format. 
            if (escape_unicode && static_cast<uint8_t>(*iter) > 0x80) {
                uint8_t uni = static_cast<uint8_t>(*iter);
                int length = 0;
                
                // Figure out how long the code point is
                if (uni >> 5 == 0x6) {
                    length = 2;
                }
                else if (uni >> 4 == 0xe) {
                    length = 3;
                }
                else if (uni >> 3 == 0x1e) {
                    length = 4;
                }

                if (iter + length > end) {
                    // invalid code point
                    return "invalid code point";
                }
                
                std::string utf8(iter, iter + length);
                std::u16string utf16 = jsonex::util::__codecvt_utf8_to_utf16(utf8);
                escape = "";
                char _hex_table[16] = {'0','1','2','3','4','5','6','7',
                                       '8','9','a','b','c','d','e','f'};
                for (auto it = utf16.begin(); it != utf16.end(); it++) {
                    escape += "\\u";
                    escape += _hex_table[(*it & 0xF000) >> 12];
                    escape += _hex_table[(*it & 0xF00) >> 8];
                    escape += _hex_table[(*it & 0xF0) >> 4];
                    escape += _hex_table[(*it & 0xF)];
                }

                processed.append(start, iter);
                processed.append(escape);
                iter += length;
            }
            else {
                switch (*iter) {
                    case '"':
                        escape = "\\\"";
                        break;
                    case '\\':
                        escape = "\\\\";
                        break;
                    case '/':
                        escape = "\\/";
                        break;
                    case '\b':
                        escape = "\\b";
                        break;
                    case '\f':
                        escape = "\\f";
                        break;
                    case '\n':
                        escape = "\\n";
                        break;
                    case '\r':
                        escape = "\\r";
                        break;
                    case '\t':
                        escape = "\\t";
                        break;
                    default:
                        iter++;
                        continue;
                }
                processed.append(start, iter);
                processed.append(escape);
                iter++;
            }
            
            start = iter;
        }

        processed.append(start, iter);
        
        return processed; 
    }
    
    string __codecvt_utf16_to_utf8(const u16string &code_points) {
        std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> utf16_to_utf8;
        
        return utf16_to_utf8.to_bytes(code_points);
    }

    u16string __codecvt_utf8_to_utf16(const string &str) {
        std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> utf16_to_utf8;
        return utf16_to_utf8.from_bytes(str);
    }
    
    string __codecvt_utf8_to_utf16_byte_string(const string &str) {
        std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> utf16_to_utf8;
        u16string tmp = utf16_to_utf8.from_bytes(str);
        // convert 2-byte string to narrow string
        return string((char *)tmp.c_str(), tmp.length() * sizeof(char16_t)/sizeof(char));
    }


}}
