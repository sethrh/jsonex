//
//  jsonex_utilities.h
//  json_experiment
//
//  Created by Seth Hill on 3/2/14.
//  Copyright (c) 2014 Seth Hill. All rights reserved.
//

#ifndef jsonex_utilities_h
#define jsonex_utilities_h

#include <string>
#include <vector>

namespace jsonex { namespace util {

    /*
    size of a JSON object:
    {"x":x} 7 chars minimum per object
    {"":[x]} 9 chars minimum per array
    */
    
    using std::string;
    using std::u16string;
    using std::vector;
    
    
    string::size_type size_sum(const string::size_type &a, const string &b);
    
    string join(vector<string> vec, string sep);
    
    string json_to_utf8(const string &json);
    string utf8_to_json(const string &str, bool escape_unicode=false);
    
    string __codecvt_utf16_to_utf8(const u16string &code_points);
    u16string __codecvt_utf8_to_utf16(const string &str);
    string __codecvt_utf8_to_utf16_byte_string(const string &str);
    
}}


#endif
