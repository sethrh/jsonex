//
//  jsonrpc_helper.h
//  json_experiment
//
//  Created by Seth Hill on 3/2/14.
//  Copyright (c) 2014 Seth Hill. All rights reserved.
//

#ifndef jsonrpc_helper_h
#define jsonrpc_helper_h

#include <string>
#include <vector> 

#include "json_experiment.h"


namespace jsonrpc {
    using std::pair;
    using std::string;
    using std::vector;

    /** Create a jsonrpc method call with an empty params value */
    jsonex::object create_rpc_call(const std::string method_name, const std::string id) {
        jsonex::object call;
        
        jsonex::object o2{
            {"jsonrpc", "2.0"},
            {"method", method_name},
            {"id", id},
            {"params", ""}
        };

        return call;
    }

    
    /** Return true of the specified object is a method call */
    bool is_method_call(jsonex::object &o) {
        jsonex::object_reference child = o.get_child("jsonrpc");
        if (!child || *child != "2.0") {
            return false;
        }
        
        child = o.get_child("method");
        if (!child) {
            return false;
        }
        
        child = o.get_child("id");
        if (!child) {
            return false;
        }
        
        return true;
    }


}

#endif
